# -*- coding: iso-8859-1 -*-
#
# This is approximately the configuration in use for the NDS community wiki
#
"""MoinMoin Desktop Edition (MMDE) - Configuration

ONLY to be used for MMDE - if you run a personal wiki on your notebook or PC.

This is NOT intended for internet or server or multiuser use due to relaxed security settings!
"""

import sys, os, re

from MoinMoin.config import multiconfig, url_prefix_static
from MoinMoin.util.abuse import log_attempt

wikihome = os.environ.get("HOME")
if wikihome:
    sys.path.insert(0, os.path.join(wikihome, "nds-wiki"))

class LocalConfig(multiconfig.DefaultConfig):
    # vvv DON'T TOUCH THIS EXCEPT IF YOU KNOW WHAT YOU DO vvv
    # Directory containing THIS wikiconfig:
    wikiconfig_dir = os.path.abspath(os.path.dirname(__file__))

    # We assume this structure for a simple "unpack and run" scenario:
    # wikiconfig.py
    # wiki/
    #      data/
    #      underlay/
    # If that's not true, feel free to just set instance_dir to the real path
    # where data/ and underlay/ is located:
    #instance_dir = '/where/ever/your/instance/is'
    instance_dir = os.path.join(wikiconfig_dir, 'wiki')

    # Where your own wiki pages are (make regular backups of this directory):
    data_dir = os.path.join(instance_dir, 'data', '') # path with trailing /

    # Where system and help pages are (you may exclude this from backup):
    data_underlay_dir = os.path.join(instance_dir, 'underlay', '') # path with trailing /

    DesktopEdition = False # give all local users full powers
    acl_rights_default = u"Known:read,write,delete,revert All:read"
    surge_action_limits = None # no surge protection
    sitename = u'NDS'
    logo_string = u'<img src="%s/common/wikilogo.png" alt="NDSWiki">' % url_prefix_static
    # ^^^ DON'T TOUCH THIS EXCEPT IF YOU KNOW WHAT YOU DO ^^^

    #page_front_page = u'FrontPage' # change to some better value

    superuser = [u'RayPlante']
    page_front_page = u"NDSwiki"
    # page_front_page = u"FrontPage"

    # Add your configuration items here.
    secrets = 'replaceWithARandomString' 

    theme_default = 'nds'

    mail_from = 'Ray Plante <rplante@ncsa.illinois.edu>'
    # mail_from = 'NDS Info <info@nationaldataservice.org>'
    # mail_login = None
    mail_smarthost = 'smtp.ncsa.uiuc.edu'

    bad_emails = [ r'mailcatch.com$', r'pixymix.com', r'mylongemail.*.info$',
                   r'^armindawestliegv3177', r'^kimberbarclayik6904',
                   r'hop2.xyz$', r'spamavert.com$', r'mynewemail.*.info$',
                   r'bumil.org$', r'decisivetalk.com', r'nonspammer.de$', 
                   r'veryrealemail.com$', r'interforexclub.com$', 
                   r'nonspam.eu$', r'believe-ro.com$', r'mail.bg$', 
                   r'manifestgenerator.com$'
    ]

    def newaccount_review(self, request, user):
        """
        This implementation is meant to shut-out spammers.  The vague 
        error message is intentional.
        """
        abuser = "Unsupported profile type; please contact administrator."

        if filter(lambda b: re.search(b, user.email), self.bad_emails):
            log_attempt("newaccount review (disallowed email)", False, request, 
                        "%s <%s>" % (user.name, user.email))
            return abuser

        return None

    # the page used by a superuser to approve all acccounts in need of 
    # activation
    # (*)
    activation_super_approve_page = 'AccountApproval'

    # the page used by a non-superuser (typically a new user but can be
    # anyone with a token) to activate an account.
    # (*)
    activation_activate_page = 'AccountVerification'

    ##################################################################
    # the user must verify their email address 
    #
    newaccount_activation = 'verify'

    # template for email sent to user containing token and instructions
    # for verifying receipt of the email.
    # 
    activation_verify_email_template = 'verify' 

    # the template for the email that goes to the user informing them that
    # their account has been activated.  
    # (*)
    activation_activated_email_template = "verified"

    # the template for the email that goes to the user informing them that
    # they successfully dropped their account.  
    # 
    activation_dropped_email_template = "dropped"

    # if set to 1, a successful activation by a non-super user will take 
    # the user immediately to the login page.  
    # (*)
    activation_go_to_login = 1

    # the amount of time (in days) to allow a user to verify their email
    # address after creating the account.  If not set, there will be no 
    # expiration.  It is recommended that you set this.  
    # (*)
    def activation_verify_expires_days(self, request, user):
        if user.email.endswith("mailcatch.com"):
            return 0.001
        if user.email.endswith(".edu"):
            return 4
        return 0.2

    # the page used by a superuser to approve all acccounts in need of 
    # activation
    # (*)
    activation_super_approve_page = 'AccountApproval'

    # the page used by a non-superuser (typically a new user but can be
    # anyone with a token) to activate an account.
    # (*)
    activation_activate_page = 'AccountVerification'

    newaccount_textchas = {
        'en': {
#            u'question':
#                u'answer',
        }
    }


# DEVELOPERS! Do not add your configuration items there,
# you could accidentally commit them! Instead, create a
# wikiconfig_local.py file containing this:
#
# from wikiconfig import LocalConfig
#
# class Config(LocalConfig):
#     configuration_item_1 = 'value1'
#

try:
    from wikiconfig_local import Config
except ImportError, err:
    if not str(err).endswith('wikiconfig_local'):
        raise
    Config = LocalConfig

