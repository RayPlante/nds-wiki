"""
An OAuth 1 authenticator for MoinMoin
"""
from MoinMoin import log
logging = log.getLogger(__name__)

from urlparse import urlparse, urlunparse
import os

from MoinMoin import user
from MoinMoin.auth import BaseAuth
from MoinMoin.auth import CancelLogin, ContinueLogin
from MoinMoin.auth import MultistageFormLogin, MultistageRedirectLogin
from MoinMoin.auth import get_multistage_continuation_url
from MoinMoin.util.abuse import log_attempt

from requests_oauthlib import OAuth1Session
from requests_oauthlib.oauth1_session import TokenRequestDenied

test_client_key = "k"
test_client_secret = "s"

def _log_attempt(system, success, request=None, user_obj=None):
    username=None
    if user_obj:
        if user_obj.username:
            username = user_obj.username
        elif user_obj.auth_username:
            username = user_obj.auth_username
    log_attempt(system, success, request, username)


class OAuth1(BaseAuth):
    """
    an authentication module that leverages OAuth v.1a interface to a web-based 
    identity provider (IP).  This module assumes that the IP provides an OAuth
    protected service that will return the user identity associated the 
    OAuth access token provided to it.  The returned data and its format is 
    not assumed but rather handled by an IP-specific function provided at 
    construction.

    Thus, to use this module, one needs the following at configuration time:
       1. The IP's OAuth protocol endpoint URLs
       2. The IP's identity retrieval service endpoint
       3. A valid OAuth client token and secret provided by the IP
       4. A function that can process the data return by the IP's identity
          retrieval service

    The identity handling function...[requirements]
    """
    name = 'oath1'
    loginsystem = name+'/login'
    logoutsystem = name+'/logout'
    requestsystem = name+'/request'
    logout_possible = True

    ok_schemes = ("http", "https")

    def __init__(self, server_url_base, client_key=test_client_key, 
                 client_secret=test_client_secret, id_handler=None,
                 initiate_url="initiate", auth_url="/delegate", 
                 token_url="token", id_url="getcert",require_success=True):
        """
        create the OAuth1-based authentication module.  The client_key
        and client_secret are obtained out-of-band from the OAuth1 server
        and provided as part of the MoinMoin configuration.

        @param server_url_base: the base URL for server OAuth1 endpoints
        @param id_handler: a function that processes the identity data 
                  returned by a call to the id_url.  It should accept a 
                  a MoinMoin.user.User object and the 
                  data object returned by the call; it should return a 
                  revised User object or None if the identity data cannot 
                  be matched to a known user.  Authentication will be 
                  considered successful if the returned user.valid is True.
        @param client_key:  the unique client token provided to this MoinMoin
                  instance by the OAuth1 server
        @param client_secret:  the client token's associated secret 
        @param initiate_url:  The server's OAuth1 endpoint URL for initiating
                  the OAuth process by retrieving the request token (also 
                  referred to as the "temporary credential request" URL).  If 
                  it is a relative URL it is combined with the server_url_base 
                  to obtain the absolute URL.  A leading slash (/) will cause 
                  the URL path in the server_url_base to be disregarded (i.e.
                  it is relative to the server's "home" URL).  
        @param auth_url:  the server's OAuth1 endpoint URL for accessing the 
                  login page to authenticate the process (also referred to as 
                  the "resource owner authorirzation" URL).  It is combined 
                  with the server_url_base just as initiate_url.
        @param token_url:  the server's OAuth1 endpoint URL for retrieving 
                  the authorization token (also referred to as the "token 
                  request" URL).  It is combined 
                  with the server_url_base just as initiate_url.
        @param id_url:  the server's endpoint for retrieving the user's 
                  identity.  This endpoint accepts the authorization token
                  like a standard OAuth1 protected resource.  It is combined 
                  with the server_url_base just as initiate_url.
        @param require_success:  boolean indicating whether 
                  successful authentication by this module should necessary
                  to continue with the whole authentication chain.  If True
                  (default), protocol and user failures during login() will 
                  cause a CancelLogin to be returned; otherwise, simply 
                  ContinueLogin will be returned.  Set this to False if 
                  this module is part of a chain of modules to be OR-ed 
                  together in which success in any one of them is sufficient
                  for successful authentication.
        """
        BaseAuth.__init__(self)
        self._ckey = client_key
        self._csecret = client_secret
        self._identity_handler = id_handler
        self._success_required = require_success

        self.base = server_url_base
        self._init_url = self._urljoin(server_url_base, initiate_url)
        self._auth_url = self._urljoin(server_url_base, auth_url)
        self._tok_url  = self._urljoin(server_url_base, token_url)
        self._id_url   = self._urljoin(server_url_base, id_url)
        
    def _urljoin(self, base, url, require_scheme=None):
        if not require_scheme:
            require_scheme = self.ok_schemes
        if not hasattr(require_scheme, "__iter__"):
            require_scheme = [require_scheme]

        uparts = urlparse(url)
        if uparts.scheme:
            if uparts.scheme not in require_scheme:
                raise ValueError("_urljoin(): unsupported scheme in absolute url: "+url)
            if uparts.scheme != "file" and not uparts.netloc:
                raise ValueError("_urljoin(): bad host in absolute url: "+url)
            return url

        out = list(urlparse(base))
        if len(uparts.path) > 0:
            if uparts.path[0] == '/':
                out[2] = uparts.path
            else:
                out[2] = os.path.join(out[2], uparts.path)
        out[3] = uparts.params
        out[4] = uparts.query
        out[5] = uparts.fragment

        return urlunparse(out)

    def _quit_login(self, user_obj, message=None):
        if self._success_required:
            return CancelLogin(message)
        return ContinueLogin(user_obj, message)

    def login(self, request, user_obj, **kw):
        """
        Handle a login request.  

        To complete the authentication process, this function will get called 
        twice.  The first time through, this function will redirect the user 
        to the OAuth server login form.  When the user submits that form, the 
        user will get redirected back to the MoinMoin site and this function 
        is called the second time.  URL arguments provide the state information
        for distinguishing which step the call corresponds to: a "stage" 
        argument (set to the value of self.name) indicates that this is the 
        second call to the login function.  Other arguments provide the data 
        for verifying the successful authorization to login.  
        """
        continuation = kw.get('multistage')

        if continuation:
            logging.debug("%s authentication step 2/2 begins", self.name)

            # handle post-login-page processing: verify authentication and 
            # determining the user's identity
            return self._handle_continuation(request, user_obj)

        logging.debug("%s authentication step 1/2 begins", self.name)

        # A valid user indicated successful authentication previously 
        # (e.g. via a cookie); we only need to authenticate the user if 
        # we are being forced to.  
        # Note that this module can sit within a chain of authentication 
        # modules; other modules may get run regardless of the outcome of 
        # this login attempt.
        if user_obj and user_obj.valid and not kw.get('force_auth'):
             return ContinueLogin(user_obj)

        # short-hand for the message translator
        _ = request.getText

        # We need an anonymous session (cookie) to store authentication 
        # data between calls to this function.  Note that this state gets 
        # stored in a temporary file.  
        if not request.cfg.cookie_lifetime[0]:
            logging.error('Anonymous sessions required but not enabled '+
                          'for %s authentication', self.name)
            return self._quit_login(user_obj,
                                 _('Insufficient configuration for OAuth-based login.'))

        # determine the callback URL
        callback_url = get_multistage_continuation_url(request, self.name)
        logging.debug('Using login callback: %s', callback_url)

        oauth = OAuth1Session(self._ckey, client_secret=self._csecret, 
                              callback_uri=callback_url)
        try:
            authreq = oauth.fetch_request_token(self._init_url)
            if not authreq or not authreq.get('oauth_token'):
                # don't think this is possible; TokenMissing would be thrown
                logging.error("OAuth initiate failure: incomplete response: "
                              + str(authreq))
                return self._quit_login(user_obj, _('OAuth1 server response error'))

            # save the request token,secret with the session.  (This gets 
            # cached to a file.)
            request.session['oauth1_request_token'] = authreq.get('oauth_token')
            request.session['oauth1_request_secret'] = \
                authreq.get('oauth_token_secret')

            # decorate the authorization endpoint with the necessary 
            # oauth data (namely the request token).
            auth_url = oauth.authorization_url(self._auth_url)

            # redirect the user to the login page
            logging.debug("Redirecting to login page: %s", auth_url)
            return MultistageRedirectLogin(auth_url)
        except TokenRequestDenied, ex:
            if ex.status_code == 404:
                logging.error('OAuth request endpoint not found (404): '+
                              self._init_url)
            else:
                logging.error('OAuth endpoint failure: ' +str(ex))
            return self._quit_login(user_obj, 
                             _('OAuth1 protocol failure (%d)' % ex.status_code))
                                   
        except Exception, ex:
            logging.error('OAuth1 protocol failure: %s', str(ex))
            return self._quit_login(user_obj, 
                          _('OAuth1 protocol failure; unable to request login'))

    def _handle_continuation(self, request, user_obj):
        # complete the authentication process after the user has visited 
        # the login page and is redirected back to this MoinMoin site

        _ = request.getText

        # We are relying on the MoinMoin anonymous session cookie to 
        # get the request token and secret (instead of using the returned 
        # token to retrieve the cached secret).  
        reqkey = request.session.get('oauth1_request_token')
        reqsecret = request.session.get('oauth1_request_secret')
        if not reqkey:
            logging.warning('Login session timed out: lost OAuth request token')
            return self._quit_login(user_obj, _('Login session timed out'))

        # I'm told that sometimes a verifier is (erroneously) not returned
        authverif = request.values.get('oauth_verifier')
        if not authverif:
            logging.warning('OAuth server failed to return verifier from '+
                            'login process')

        oauth = OAuth1Session(self._ckey, client_secret=self._csecret, 
                              resource_owner_key=reqkey, 
                              resource_owner_secret=reqsecret,
                              verifier=authverif)

        try:
            # pull the actual access token and secret
            authcreds = oauth.fetch_access_token(self._tok_url)

            if not authcreds.get('oauth_token'):
                # should not happen; protocol failures should raise Exceptions
                logging.warning('OAuth authorization failed: no token returned')
                return self._quit_login(user_obj, 
                     _('OAuth authorization failed (no access token returned)'))

            # retrieve the user identity
            id_data = oauth.get(self._id_url)
            if not id_data:
                # raising exception is the prefered failure response
                logging.error('Identity retrieval: no data returned')
                return self._quit_login(user_obj, 
                              _('Identity retrieval failed (no data returned)'))

            # extract the identity information and match it to an internal 
            # identity
            return self._handle_identity(request, user_obj, id_data)

        except TokenRequestDenied, ex:
            logging.warning("Authentication denied by OAuth identity provider")
            _log_attempt(loginsystem, False, request, user_obj)
            return self._quit_login(user_obj, _('Login denied by user'))
        except Exception, ex:
            logging.error('Authentication protocol failure: '+str(ex))
            return self._quit_login(user_obj, 
                             _('Authentication protocol failure (after login)'))

    def _handle_identity(self, request, user_obj, id_data):
        _ = request.getText
        user = None
        try:
            if not self._identity_handler:
                logging.warning('No identity service handler set')
                user = self._default_handler(user_obj, id_data)
            else:
                user = self._identity_handler(user_obj, id_data)

        except Exception, ex:
            return self._quit_login(user_obj, _('Unrecognized user data'))

        if not user:
            return self._quit_login(user_obj, _('Unidentified user'))
        elif not user.valid:
            _log_attempt(loginsystem, False, request, user_obj)
            return self._quit_login(user_obj, _('User Disabled'))

        # user.valid=True: success!
        _log_attempt(loginsystem, True, request, user_obj)
        return ContinueLogin(user_obj)


    def _default_handler(self, user_obj, id_data):
        if hasattr(id_data, 'content'):
            id_data = id_data.content
        logging.error('Unrecognized user data: '+str(id_data))
        return None

        
