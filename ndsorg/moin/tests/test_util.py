from __future__ import absolute_import
import sys, os, re, pytest

from OpenSSL import crypto
from .. import util as util
from MoinMoin.auth import ContinueLogin, CancelLogin, MultistageRedirectLogin

test_data = [('DC', 'org'), ('DC', 'cilogon'), ('C', 'US'), 
             ('O', 'University of Illinois at Urbana-Champaign'), 
             ('CN', 'Raymond Plante A16601')]
test_str = 'DC=org/DC=cilogon/C=US/O=University of Illinois at Urbana-Champaign/CN=Raymond Plante A16601'

test_dir = os.path.join(os.path.dirname(util.__file__), "tests")

class TestDN(object):

    def setup_method(self, method):
        self.dn = util.DN(test_data)

    def test_parse_DN_str(self):
        dn = util.DN.parse_DN_str(test_str)
        assert isinstance(dn, list)
        assert len(dn) == 5
        assert isinstance(dn[0], tuple)
        assert dn[0][0] == 'DC' and dn[0][1] == 'org'
        assert dn[1][0] == 'DC' and dn[1][1] == 'cilogon'
        assert dn[2][0] == 'C'  and dn[2][1] == 'US'
        assert dn[3][0] == 'O'  and \
            dn[3][1] == 'University of Illinois at Urbana-Champaign'
        assert dn[4][0] == 'CN' and dn[4][1] == 'Raymond Plante A16601'

    def test_get_component(self):
        assert self.dn.get_component('C') == 'US'
        assert self.dn.get_component('CN') == 'Raymond Plante A16601'
        assert self.dn.get_component('O') == \
            'University of Illinois at Urbana-Champaign'
        v = self.dn.get_component('DC')
        assert isinstance(v, list)
        assert len(v) == 2
        assert v[0] == 'org'
        assert v[1] == 'cilogon'

    def test_props(self):
        assert self.dn.countryName == 'US'
        assert self.dn.commonName == 'Raymond Plante A16601'
        assert self.dn.orgName == \
            'University of Illinois at Urbana-Champaign'
        v = self.dn.domainName
        assert isinstance(v, list)
        assert len(v) == 2
        assert v[0] == 'org'
        assert v[1] == 'cilogon'
        
    def test_components(self):
        data = self.dn.components()
        assert isinstance(data, list)
        assert len(data) == 5
        assert data == test_data

    def test_str(self):
        assert str(self.dn) == test_str

    def test_set(self):
        self.dn.set_component('ST', 'IL')
        assert self.dn.get_component('ST') == 'IL'
        data = self.dn.components()
        assert isinstance(data, list)
        assert len(data) == 6
        assert data[3][0] == 'ST'

        assert self.dn.orgUnitName == None
        self.dn.orgUnitName = 'NCSA'
        assert self.dn.orgUnitName == 'NCSA'
        self.dn.orgUnitName = ['Graduate College', 'NCSA']
        assert self.dn.orgUnitName == ['Graduate College', 'NCSA']

        self.dn.commonName = "Raymond Plante"
        assert self.dn.commonName == "Raymond Plante"
        assert self.dn.commonName == "Raymond Plante"
        self.dn.add_component('CN', 'A490')
        assert self.dn.commonName == ["Raymond Plante", "A490"]

        assert str(self.dn) == \
            'DC=org/DC=cilogon/C=US/ST=IL/O=University of Illinois at Urbana-Champaign/OU=Graduate College/OU=NCSA/CN=Raymond Plante/CN=A490'            

    def test_from_x509(self):
        with open(os.path.join(test_dir,"rplante.pem")) as fd:
            cert = fd.read()
        xdn = crypto.load_certificate(crypto.FILETYPE_PEM, cert).get_subject()
        dn = util.DN.from_X509Name(xdn)
        assert str(dn) == test_str

    def test_from_str(self):
        dn = util.DN.from_str(test_str)
        assert dn.components() == test_data

