import imp, os

from MoinMoin._tests import maketestwiki, wikiconfig
from MoinMoin.web.request import TestRequest
from MoinMoin.wsgiapp import init
from MoinMoin.Page import Page
from MoinMoin.user import User

def create_test_config(configfile=None, *kw):
    if not configfile:
        configfile = "wikiconfig_test.py"
    if configfile == os.path.basename(configfile):
        configdir = os.path.dirname(__file__)
        configfile = os.path.join(configdir, configfile)

    wikiconfig_test = imp.load_source("wikiconfig_test", configfile)
    cfg = None
    if "Config" in dir(wikiconfig_test):
        cfg = wikiconfig_test.Config
        for key in kw:
            setattr(cfg, key, kw[key])

    return cfg
    

def create_test_request(config='', static_state=[False], pagename="WikiHome",
                        qstr=None):
    if config == '':
        config = create_test_config()

    if not static_state[0]:
        try:
            maketestwiki.run(True)
        except IOError, e:
            # bug in 1.9.8: no underlay.tar; ignore it
            if 'underlay.tar' not in str(e):
                raise

        static_state[0] = True

    request = TestRequest('/'+pagename, qstr)
    request.given_config = config
    request = init(request)
    request.page = Page(request, pagename)
    return request

def create_test_user(request, token="__token__", username=u'__TestUser__', 
                     email="testuser@example.org", activated=True, **kw):
    user = User(request, name=username)
    user.token = token
    user.email = email
    user.disabled = (not activated and 2) or 0
    for key in kw:
        setattr(user, key, kw[key])

    user.save()
    user.valid = 0
    assert user.exists()
    return user

 

