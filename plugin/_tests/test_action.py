# -*- coding: iso-8859-1 -*-

"""
This tests the common activation functions found in activation/__init__.py

@copyright: 2015 MoinMoin:RayPlante
@license: GPL, see COPYING for details
"""
import os
import py.test

from .maketestwiki import create_test_request, create_test_user
from MoinMoin.user import User
from MoinMoin import wikiutil

# we have to import the action as a plugin
def import_action(request):
    cfg = request.cfg
    assert len(request.cfg._plugin_modules) > 0

    # import pdb; pdb.set_trace()
    activatef = \
       wikiutil.importWikiPlugin(cfg,"action","activate")

    return activatef

def import_activation(request):
    activatef = import_action(request)
    assert activatef

    actionmodulename = activatef.__module__.rsplit('.', 1)
    actionmodule = __import__(actionmodulename[0], globals(), {}, 
                             actionmodulename[1])
    actionmodule = getattr(actionmodule, actionmodulename[1])
    actionmodule._import_activation_module(request)
    return actionmodule.activation

def test_import():
    request = create_test_request()
    activation = import_activation(request)

    assert activation
    assert activation.get_email_template


class TestActivateAction:

    def setup_method(self, method):
        self.request = create_test_request()
        self.form = dict(self.request.args)
        self.form.update(self.request.form)

    def teardown_method(self, method):
        if self.request.user.exists():
            self.request.user.remove()

    def get_action_function(self, name=None):
        if name:
            return wikiutil.importWikiPlugin(self.request.cfg, "action", 
                                             "activate", name)

        # return execute
        return wikiutil.importWikiPlugin(self.request.cfg, "action", "activate")

    def test_drop_user(self):
        act = import_activation(self.request)
        func = self.get_action_function('drop_user')

        user = create_test_user(self.request)
        try: 
            act.set_user_token(self.request, user, 2)
            user.save()
            assert user.exists()
            assert act.needs_activation(user)
            ok, msg = func(self.request, user.id)
            assert ok == 'drop'
            assert msg
            assert not user.exists()
        finally:
            if user.exists(): user.remove()

        user = create_test_user(self.request)
        try: 
            user.disabled = 0
            assert user.exists()
            assert not act.needs_activation(user)
            ok, msg = func(self.request, user)
            assert ok == ''
            assert "activated" in msg
            assert user.exists()
        finally:
            if user.exists(): user.remove()

        # testing non-existent
        ok, msg = func(self.request, user)
        assert ok == ''
        assert "non-existent" in msg
        assert not user.exists()

    def test_activate_user(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_user')

        user = create_test_user(self.request)
        try: 
            act.set_user_token(self.request, user, 2)
            user.disabled = 1
            user.save()
            assert user.exists()
            assert not act.needs_activation(user)
            ok, msg = func(self.request, user.id)
            assert ok == ''
            assert "disabled" in msg

            user.disabled = act.AWAITING_ACTIVATION
            user.save()
            assert user.exists()
            assert act.needs_activation(user)
            ok, msg = func(self.request, user.id)
            assert ok == 'activate'
            assert "successful" in msg
            user = User(self.request, user.id)
            assert user.exists()
            assert not act.needs_activation(user)

            ok, msg = func(self.request, user.id)
            assert ok == ''
            assert "already" in msg
            user = User(self.request, user.id)
            assert user.exists()
            assert not act.needs_activation(user)

        finally:
            if user.exists(): user.remove()

    def test_super_activate_user(self):
        act = import_activation(self.request)
        func = self.get_action_function('super_activate_accounts')

        user1, user2, user3, user4, user5 = None, None, None, None, None
        try:
            # activate it
            user1 = create_test_user(self.request, username="gurnc",
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user1, 2)
            user1.save()

            # drop it
            user2 = create_test_user(self.request, username='GlibParker',
                                     activated=False, firstname="Glib", 
                                     midname="", lastname="Parker", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user2, 2)
            user2.save()

            # purge it
            user3 = create_test_user(self.request, username='jubel',
                                     activated=False, firstname="Jubel", 
                                     midname="", lastname="Early", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user3, -2)
            user3.save()

            # cancel it
            user4 = create_test_user(self.request, username="enigma",
                                     activated=False, firstname="Edward", 
                                     midname="", lastname="Nigma", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user4, 2)
            user4.save()

            # activate anyway
            user5 = create_test_user(self.request, username='hicama',
                                     activated=False, firstname="Fenton", 
                                     midname="", lastname="Hicama", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user5, -2)
            user5.save()

            self.request = create_test_request(qstr={'submit': 'submit',
                                                     user1.id: 'activate',
                                                     user2.id: 'drop',
                                                     user4.id: 'cancel',
                                                     user5.id: 'activate'})
            form = dict(self.request.args)
            form.update(self.request.form)

            ok, msg = func(self.request, form)
            assert ok == 'bulk'
            assert msg
            assert "Activated 2 users" in msg
            assert "Dropped 1 user." in msg

            user1 = User(self.request, user1.id)
            assert user1.exists()
            assert user1.disabled == 0
            user2 = User(self.request, user2.id)
            assert not user2.exists()
            user3 = User(self.request, user3.id)
            assert not user3.exists()
            user4 = User(self.request, user4.id)
            assert user4.exists()
            assert user4.disabled == 2
            user5 = User(self.request, user5.id)
            assert user5.exists()
            assert user5.disabled == 0

        finally:
            if user1 and user1.exists(): user1.remove()
            if user2 and user2.exists(): user2.remove()
            if user3 and user3.exists(): user3.remove()
            if user4 and user4.exists(): user4.remove()
            if user5 and user5.exists(): user5.remove()
        
    def test_activate_accounts_super_invalid(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')
        self.request = create_test_request(qstr={'submit': 'submit'})
        form = dict(self.request.args)
        form.update(self.request.form)

        ok, msg = func(self.request, form)
        assert ok == ''
        assert msg
        assert "rejected" in msg

        self.request.cfg.superusers = [u'__su1__']
        self.request.user = create_test_user(self.request, username="__su1__")
        self.request.user.valid = 0

    def test_activate_accounts_super(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        user1, user2 = None, None
        try:
            # activate it
            user1 = create_test_user(self.request, username="gurnc",
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user1, 2)
            user1.save()

            # drop it
            user2 = create_test_user(self.request, username='GlibParker',
                                     activated=False, firstname="Glib", 
                                     midname="", lastname="Parker", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user2, 2)
            user2.save()

            self.request = create_test_request(qstr={'submit': 'submit',
                                                     user1.id: 'activate',
                                                     user2.id: 'drop'})
            self.request.cfg.superusers = [u'__su1__']
            self.request.user = create_test_user(self.request, 
                                                 username="__su1__")
            self.request.user.valid = 1
            form = dict(self.request.args)
            form.update(self.request.form)

            ok, msg = func(self.request, form)
            assert ok == 'bulk'
            assert msg
            assert "Activated 1 user" in msg
            assert "Dropped 1 user." in msg

        finally:
            if user1 and user1.exists(): user1.remove()
            if user2 and user2.exists(): user2.remove()
        
    def test_activate_accounts_user_drop(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        user2 = None
        try:
            # drop it
            user2 = create_test_user(self.request, username='GlibParker',
                                     activated=False, firstname="Glib", 
                                     midname="", lastname="Parker", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user2, 2)
            user2.save()

            self.request = create_test_request(qstr={'submit': 'drop',
                                                     'token': user2.token})
            form = dict(self.request.args)
            form.update(self.request.form)
        
            ok, msg = func(self.request, form)
            assert ok == 'drop'
            assert msg

        finally:
            if user2 and user2.exists(): user2.remove()

    def test_activate_accounts_user_activate(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        user1 = None
        try:
            # activate it
            user1 = create_test_user(self.request, username="gurnc",
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user1, 2)
            user1.save()

            self.request = create_test_request(qstr={'submit': 'activate',
                                                     'token': user1.token})
            form = dict(self.request.args)
            form.update(self.request.form)
        
            ok, msg = func(self.request, form)
            assert ok == 'activate'
            assert msg
            assert "successful" in msg

        finally:
            if user1 and user1.exists(): user1.remove()

        
    def test_activate_accounts_user_valid(self):
        # This test shows that it's okay to be logged in when activating 
        # an account.  Anyone with the token can do it.

        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        user1 = None
        try:
            # activate it
            user1 = create_test_user(self.request, username="gurnc",
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US")
            act.set_user_token(self.request, user1, 2)
            user1.save()

            self.request = create_test_request(qstr={'submit': 'activate',
                                                     'token': user1.token})
            self.request.user = create_test_user(self.request, 
                                                 username="__su1__")
            self.request.user.valid = 1
            form = dict(self.request.args)
            form.update(self.request.form)
        
            ok, msg = func(self.request, form)
            assert ok == 'activate'
            assert msg
            assert "successful" in msg

        finally:
            if user1 and user1.exists(): user1.remove()

        
    def test_activate_accounts_user_notoken(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        self.request = create_test_request(qstr={'submit': 'activate'})
        form = dict(self.request.args)
        form.update(self.request.form)
    
        ok, msg = func(self.request, form)
        assert ok == ''
        assert msg
        assert "token required" in msg

    def test_activate_accounts_user_badtoken(self):
        act = import_activation(self.request)
        func = self.get_action_function('activate_accounts')

        self.request = create_test_request(qstr={'submit': 'activate',
                                                 'token':  'asdf'     })
        form = dict(self.request.args)
        form.update(self.request.form)
    
        ok, msg = func(self.request, form)
        assert ok == ''
        assert msg
        assert "expired" in msg

