# -*- coding: iso-8859-1 -*-

"""
This tests the common activation functions found in activation/__init__.py

@copyright: 2015 MoinMoin:RayPlante
@license: GPL, see COPYING for details
"""
import os, time
from datetime import datetime
import py.test

from .maketestwiki import create_test_request, create_test_user
from MoinMoin import wikiutil
from MoinMoin.parser.text_moin_wiki import Parser
from MoinMoin.macro import Macro

# we have to import the macro as a plugin
def import_macro(request):
    cfg = request.cfg
    assert len(request.cfg._plugin_modules) > 0

    activatef = \
       wikiutil.importWikiPlugin(cfg,"macro","AccountActivation")

    return activatef

def import_activation(request):
    activation = wikiutil.importPlugin(request.cfg, "activation", 
                                       "common", "import_module")(request.cfg)
    assert activation
    return activation

def test_import():
    request = create_test_request()
    activation = import_activation(request)
    assert activation.get_email_template

class TestActivationInterface:

    def setup_method(self, method):
        self.request = create_test_request()
        self.form = dict(self.request.args)
        self.form.update(self.request.form)

    def teardown_method(self, method):
        if self.request.user.exists():
            self.request.user.remove()

    def get_macro_function(self, name=None):
        if name:
            return wikiutil.importWikiPlugin(self.request.cfg, "macro", 
                                             "AccountActivation", name)

        # return execute()
        return wikiutil.importWikiPlugin(self.request.cfg, "macro", 
                                         "AccountActivation")


    def test_anon_super_activation_interface(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        func = self.get_macro_function('super_activation_interface')
        assert func

        html = func(self.request, self.form)
        # import pdb; pdb.set_trace()
        assert len(html) > 0
        assert '%(' not in html
        assert '<form ' not in html
        assert 'superuser' in html
        assert '<table' not in html

    def test_invalid_super_activation_interface(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.superusers = [u'__su1__']
        self.request.user = create_test_user(self.request, username="__su1__")
        func = self.get_macro_function('super_activation_interface')
        assert func

        html = func(self.request, self.form)
        assert len(html) > 0
        assert '%(' not in html
        assert 'superuser' in html
        assert '<table' not in html
        assert '<form' not in html

    def test_super_activation_interface_no_users(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.superusers = [u'__su2__']
        self.request.user = create_test_user(self.request, username="__su2__")
        self.request.user.valid = 1
        func = self.get_macro_function('super_activation_interface')
        assert func

        html = func(self.request, self.form)
        assert len(html) > 0
        assert '%(' not in html
        assert '<form action="/WikiHome?action=activate"' in html
        assert 'superuser' in html
        assert '<table' in html

        # no users listed 
        assert 'type="radio"' not in html

    def test_super_activation_interface(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.superusers = [u'__su3__']
        self.request.user = create_test_user(self.request, username="__su3__")
        self.request.user.valid = 1
        func = self.get_macro_function('super_activation_interface')
        assert func

        activation = import_activation(self.request)
        expires = activation.token_expires_str(2)
        user1, user2 = None, None
        try:
            user1 = create_test_user(self.request, token="__tk__", 
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US",
                                     token_expires=expires)
            user2 = create_test_user(self.request, token="__tk__", 
                                     activated=False, firstname="Glib", 
                                     midname="", lastname="Parker", 
                                     institute="UIUC", country="US",
                                     token_expires=expires)

            html = func(self.request, self.form)
            assert len(html) > 0
            assert '%(' not in html
            assert '<form action="/WikiHome?action=activate"' in html
            assert 'superuser' in html
            assert '<table' in html
            assert 'type="radio"' in html
            assert 'value="drop"' in html
            assert 'value="spam"' in html

            # 3 table rows, including header
            assert len(filter(lambda l: '<tr>' in l, html.split('\n'))) == 3
                              
        finally:
            if user1: user1.remove()
            if user2: user2.remove()

    def test_user_activation_interface(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.activation_verify_expires_days = 3
        user = create_test_user(self.request, token="__tk__",
                                firstname="Gurn", midname="",
                                lastname="Cranston")
        activation = import_activation(self.request)
        activation.set_user_token(self.request, user)
        user.save()

        try:
            func = self.get_macro_function('user_activation_interface')
            assert func

            html = func(self.request, self.form, user.token)
            assert len(html) > 0
            assert '%(' not in html
            assert '<form action="/WikiHome?action=activate"' in html
            assert 'Email:' in html
            assert 'name="submit" value="Activate"' in html
        finally:
            user.remove()

    def test_badtoken_user_activation_interface(self):
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.activation_verify_expires_days = 3
        self.request.user = create_test_user(self.request, token="__tk__",
                                             firstname="Gurn", midname="",
                                             lastname="Cranston")
        func = self.get_macro_function('user_activation_interface')
        assert func

        html = func(self.request, self.form, "asdf")
        # import pdb; pdb.set_trace()
        assert len(html) > 0
        assert '%(' not in html
        assert 'expired' in html

    def test_activation_request(self):
        self.request = create_test_request()
        self.form = dict(self.request.args)
        self.form.update(self.request.form)

        func = self.get_macro_function('activation_request')
        assert func

        html = func(self.request)
        assert len(html) > 0
        assert '%(' not in html
        assert '<form action="/WikiHome?action=activate"' in html
        assert ' token ' in html
        assert 'type="text"' in html

    def test_activation_interface_su(self):
        func = self.get_macro_function('activation_interface')
        assert func

        self.request = create_test_request(qstr={'submit': 'activate'})
        self.form = dict(self.request.args)
        self.form.update(self.request.form)
        self.request.cfg.superusers = [u'__su4__']
        self.request.user = create_test_user(self.request, username="__su4__")
        self.request.user.valid = 1

        # test branch to super user
        html = func(self.request, self.form)
        assert 'superuser' in html
        assert '<table' in html
        assert 'expired' not in html
        assert 'Email:' not in html
        assert ' token ' not in html

        # test branch for invalid user, no token (same as anon)
        self.request.user.valid=0
        html = func(self.request, self.form)
        assert 'type="text"' in html
        assert ' token ' in html
        assert 'expired' not in html
        assert 'superuser' not in html
        assert '<table' not in html
        assert 'Email:' not in html

    def test_activation_interface_user(self):
        func = self.get_macro_function('activation_interface')
        assert func

        # test anonymous branch, no token
        self.request = create_test_request(qstr={'submit': 'activate'})
        self.request.cfg.activation_verify_expires_days = 3
        html = func(self.request, self.form)
        assert ' token ' in html
        assert 'type="text"' in html
        assert 'expired' not in html
        assert 'Email:' not in html
        assert 'superuser' not in html
        assert '<table' not in html

    def test_activation_interface_bt(self):
        func = self.get_macro_function('activation_interface')
        assert func

        # test anonymous branch with bad token
        self.request = create_test_request(qstr={'submit': 'activate',
                                                 'token': 'asdf'})
        self.form = dict(self.request.args)
        self.form.update(self.request.form)
        self.request.cfg.activation_verify_expires_days = 3
        html = func(self.request, self.form)
        assert 'expired' in html
        assert ' token ' in html
        assert 'type="text"' not in html
        assert 'Email:' not in html
        assert 'superuser' not in html
        assert '<table' not in html

    def test_activation_interface_gt(self):
        func = self.get_macro_function('activation_interface')
        assert func

        # test anonymous branch with good token
        self.request = create_test_request(qstr={'submit': 'activate',
                                                 'token': '__tk__'})
        self.form = dict(self.request.args)
        self.form.update(self.request.form)
        user = create_test_user(self.request, token="__tk__", activated=False,
                                firstname="Gurn", midname="",
                                lastname="Cranston")
        try:
            html = func(self.request, self.form)
            assert 'Email:' in html
            assert 'name="submit" value="Activate"' in html
            assert 'expired' not in html
            assert ' token ' not in html
            assert 'type="text"' not in html
            assert 'superuser' not in html
            assert '<table' not in html
        finally:
            user.remove()

    def test_execute(self):
        func = self.get_macro_function()
        assert func

        parser = Parser("<<AccountActivation>>", self.request)
        macro = Macro(parser)

        html = func(macro)
        assert ' token ' in html
        assert 'type="text"' in html
        assert 'expired' not in html
        assert 'Email:' not in html
        assert 'superuser' not in html
        assert '<table' not in html
        
