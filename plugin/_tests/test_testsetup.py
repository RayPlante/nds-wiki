from _tests import maketestwiki

def test_config():
    cfg = maketestwiki.create_test_config()

    assert cfg.mail_from == u'Wiki Admin <adm@mo.in>'
    assert cfg.page_front_page == u"TestWiki"

    assert cfg.secrets

def test_request():
    request = maketestwiki.create_test_request()
    assert request
    assert request.cfg
    assert request.cfg.page_front_page == u"TestWiki"
    assert len(request.cfg._plugin_modules) > 0

def test_user():
    request = maketestwiki.create_test_request()
    user = maketestwiki.create_test_user(request)
    try: 
        user.name == u'__TestUser__'
        assert user.exists()
    finally:
        user.remove()

