import os
from wikiconfig import LocalConfig

TEST_PLUGIN_DIR = os.path.dirname(os.path.dirname(__file__))

class Config(LocalConfig):

    page_front_page = u"TestWiki"

    mail_from = u'Wiki Admin <adm@mo.in>'

    plugin_dir = TEST_PLUGIN_DIR

