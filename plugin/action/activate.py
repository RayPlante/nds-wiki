"""
    MoinMoin - activate action plugin: process the submission of 
               activation requests (usually via the form provided
               by the AccountActivation macro).  

    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

"""
import datetime
import MoinMoin.user as userutil
from MoinMoin.Page import Page
from MoinMoin import log, wikiutil
from werkzeug.datastructures import MultiDict
logging = log.getLogger(__name__)

# If the activation plugin is ever integrated into the mainline MoinMoin,
# replace the next line with:
#
# from MoinMoin import activation
activation = None

def execute(pagename, request):
    """
    process an account activation action.  
    """
    _ = request.getText
    form = dict(request.args)
    form.update(request.form)

    # determine if this is a request from a new user or a superuser and 
    # set the page to show accordingly
    showpage = getattr(request.cfg, 'activation_activate_page', None)
    if request.user.valid and request.user.isSuperUser():
        showpage = getattr(request.cfg,'activation_super_approve_page',showpage)

    if not showpage:
        logging.error("activation_activate_page is not set in the configuration!")

    sendtologin = getattr(request.cfg,'activation_go_to_login',None)

    ok = "na"
    msg = _("")
    submit = form.get('submit',[''])[0]

    # import pdb; pdb.set_trace()
    # Lookup requests to look up a token; let the showpage handle that
    if submit and submit != 'Lookup':
        # this signals that there are some activations to process
        ok, msg = activate_accounts(request, form)

        # we have consumed the paramters, now clear them out 
        request.form = request.parameter_storage_class()
        request.args = request.parameter_storage_class()

    request.theme.add_msg(msg)
    if ok == 'activate' and sendtologin:
        # go to a login page
        try:
            loginexec = wikiutil.importPlugin(request.cfg,'action','login')
            loginexec(pagename, request)
            return
        except wikiutil.PluginMissingError, e:
            pass
    if ok in ('drop', 'cancel', 'activate', 'spam'):
        # go to the original page
        request.page.send_page()
    elif not showpage:
        logging.error("Unable to display the activate view due to config "+
                      "problem!")
        msg = _("Trouble displaying activate information; please contact administrator")
        request.theme.add_msg(msg)
        request.page.send_page()
    else:
        # show the approval page
        Page(request, showpage).send_page()

def activate_accounts(request, form):
    """
    process the submission of activation requests
    @rtype pair
    @return            a 2-element tuple where the first is the result status
                       and the second is the displayable user message.  The 
                       status value is a word indicating the successful 
                       operation ('activate', 'drop', 'cancel' or 'bulk') or 
                       an empty string on failure (allowing it to be treated
                       like a boolean).
    """
    _ = request.getText
    _import_activation_module(request)

    # determine the post-activation function
    postact = None
    activation_type = getattr(request.cfg, 'newaccount_activation', None)
    if activation_type:
        try:
            postact = wikiutil.importPlugin(request.cfg, "activation", 
                                            activation_type, 'on_activate')
        except wikiutil.PluginMissingError, e:
            # a post-activation function is not required
            pass

    submittype = form.get('submit', [''])[0].lower()
    if submittype == 'submit' or submittype == 'super':
        # this is a bulk activation submission which requires superuser status
        if not request.user.valid or not request.user.isSuperUser():
            return '', _("Submission rejected.  You must be logged in as a " + 
                         "superuser to activate other people's accounts.")

        return super_activate_accounts(request, form, postact)

    if submittype == 'activate' or submittype == 'drop':
        token = form.get('token', [None])[0]
        if not token:
            return '', _("Verification token required to update an account")

        uid = activation.get_user_by_token(request, token)

        if submittype == 'activate':

            if not uid:
                lifetime = activation.get_token_lifetime(request, request.user)
                logging.warn("Invalid or expired submitted token for " +
                             "activation: " + token)
                return '', _("Invalid or expired token provided.  Please " +
                             "re-create your account and activate it within " +
                             "%s days.") % \
                       getattr(request.cfg, 'activation_verify_expires_days', 5)

            return activate_user(request, uid, postact)

        else:
            if not uid:
                logging.warn("Invalid or expired submitted token for " +
                             "dropping: " + token)
                return '', _("Invalid or expired token provided.  The  " +
                             "account is already deleted.")

            return drop_user(request, uid, postact)

    if submittype and submittype != "cancel":
        logging.warn("Unrecognized activation submit command requested: " +
                     submittype)
        return '', _("Unrecognized activation submit command: %s; " +
                     "request canceld") % submittype

    return 'cancel', _("User account not activated as per your request.")

def super_activate_accounts(request, form, postactfunc=None):
    """
    process the bulk activation requests from a superuser.  This will also  
    purge any expired accounts.  Note that this does not ensure that the 
    user is a superuser.  

    @param request:     the current wiki request object
    @param form:        a dictionary of user-supplied activation parameters.  
    @param postactfunc: a function to call after the completion of the 
                           handling of one user; the signature must match 
                           the following:  postactfunc(request, user, result)
    @rtype pair
    @return             a 2-element tuple where the first is the result status
                        (either 'bulk' for success or '' for failure), and 
                       the second is the displayable user message.
    """
    _import_activation_module(request)

    all_users = [ userutil.User(request, id=userid)
                  for userid in userutil.getUserList(request) ]

    activated = []
    purgeable = []
    dropped = []
    for user in all_users:
        requested = form.get(user.id, [''])[0]
        if requested:
            if requested == 'activate':
                ok, msg = activate_user(request, user, postactfunc)
                if ok:
                    activated.append( (user.name, user.id, user.email) )
            elif requested == 'drop':
                ok, msg = drop_user(request, user, postactfunc)
                if ok:
                    dropped.append( (user.name, user.id, user.email) )
            elif requested == 'spam':
                # send a message to the log indicating that an administrator
                # marked this account request as the product of spammers.  
                # An external process (e.g. swatch) can react how to the 
                # spam (e.g. block the source IP address).
                _mark_as_spam(request, user)
                ok, msg = drop_user(request, user, postactfunc)
                if ok:
                    dropped.append( (user.name, user.id, user.email) )

        elif activation.token_expired(user):
            purgeable.append(user)

    out = ''
    if len(activated) > 0:
        msg = "Activated %s user%s" % \
                     (len(activated), (len(activated) != 1 and "s") or '')
        out += msg
        logging.info(msg + ":\n\t" + 
                     "\n\t".join(["%s (%s) <%s>" % u for u in activated]))
    if len(dropped) > 0:
        msg = "Dropped %s user%s" % \
                     (len(dropped), (len(dropped) != 1 and "s") or '')
        if out: out += "; "
        out += msg
        logging.info(msg + ":\n\t" + 
                     "\n\t".join(["%s (%s) <%s>" % u for u in dropped]))
    if out:
        out += "."
    else:
        out = "No user accounts updated (as per request)."

    activation.purge_expired_users(request, purgeable)

    return 'bulk', out

def activate_user(request, user, postactfunc=None):
    """
    update the user attributes corresponding to the given user identifier
    so as to enable use of their account.

    The postact function is called with result='activate' if the user account 
    is successfully activated.  (If a system error occurs to prevent 
    activation, the function is not called.)  This function is intended 
    to come from the activation plugin as the function 'on_activate()'.

    @param request:    the current wiki request object
    @param user:       either the identifier for the user account to be 
                       activated or a MoinMoin.User.user object.
    @param postactfunc: a function to call after the completion of the 
                           handling of one user; the signature must match 
                           the following:  postactfunc(request, user, result)
    @rtype pair
    @return            a 2-element tuple where the first is the result status
                       (either 'activate' for success or '' for failure), and 
                       the second is the displayable user message.
    """
    _import_activation_module(request)
    _ = request.getText
    if not isinstance(user, userutil.User):
        user = userutil.User(request, user)

    if activation.disabled(user):
        logging.warn('Activation request on disabled user: '+
                     user.name)
        return '', _("The account for user %s is currently disabled; you " +
                        "can not use this account to log in.") % user.name
    if user.disabled != activation.AWAITING_ACTIVATION:
        logging.warn('Activation request on already activated user: '+
                     user.name)
        return '', _("User %s is already activated; you may now log in as " +
                        "this user") % user.name

    activation.activate_user(request, user)
    if postactfunc:
        postactfunc(request, user, 'activate')

    loginurl = request.page.url(request, querystr={"action": "login"})
    return 'activate', _('Account activation successful; you may now ' +
                         '<a href="%s">login</a>.') % loginurl

def drop_user(request, user, postactfunc=None):
    """
    drop a user's account.  

    The postact function is called with result='drop' if the user account 
    is successfully dropped.  (If a system error occurs to prevent 
    activation, the function is not called.)  This function is intended 
    to come from the activation plugin as the function 'on_activate()'.

    @param request:    the current wiki request object
    @param user:       either the identifier for the user account to be 
                       dropped or a MoinMoin.User.user object.
    @param postactfunc: a function to call after the completion of the 
                           handling of one user; the signature must match 
                           the following:  postactfunc(request, user, result)
    @rtype pair
    @return            a 2-element tuple where the first is the result status
                       (either 'drop' for success or '' for failure), and 
                       the second is the displayable user message.
    """
    _import_activation_module(request)
    _ = request.getText

    if not isinstance(user, userutil.User):
        user = userutil.User(request, user)
    if not user.exists():
        return '', _("Ignored attempt to drop a non-existent user.")

    try:
        activation.drop_user(request, user, force=False)
    except RuntimeError, e:
        logging.exception(e)
        return '', _("Ignored attempt to drop an activated account.")

    if postactfunc:
        postactfunc(request, user, 'drop')

    return 'drop', "Account drop successful; the account has been removed."

def _mark_as_spam(request, user):
    ip = getattr(user, 'registration_ip', 'unknown')
    pagename = request.page.page_name
    msg = """: %s: status failed: username "%s": ip %s: page %s"""
    logging.log(logging.WARNING, msg, "newaccount approval (marked spam)", 
                user.name, ip, pagename)

#
# this function is not needed if this macro is made built-in
#    
def _import_activation_module(request):
    global activation
    if not activation:
        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)

