"""
replacement action plugin for NDS Authentication
"""

import MoinMoin.userform.login as loginform
from MoinMoin.action.login import LoginHandler
from MoinMoin.widget import html

def execute(pagename, request):
    return NDSLoginHandler(pagename, request).handle()

class NDSLoginHandler(LoginHandler):
    def __init__(self, pagename, request):
        LoginHandler.__init__(self,  pagename, request):

    def handle(self):
        # this is the implementation of LoginHandler save for the line
        # getting a custom login form

        _ = self._
        request = self.request
        form = request.values

        error = None

        islogin = form.get('login', '')

        if islogin: # user pressed login button
            if request._login_multistage:
                return self.handle_multistage()
            if hasattr(request, '_login_messages'):
                for msg in request._login_messages:
                    request.theme.add_msg(wikiutil.escape(msg), "error")
            return self.page.send_page()

        else: # show login form
            request.theme.send_title(_("Login"), pagename=self.pagename)
            # Start content (important for RTL support)
            request.write(request.formatter.startContent("content"))

            # get the custom login content
            request.write(self.getLogin(request))

            request.write(request.formatter.endContent())
            request.theme.send_footer(self.pagename)
            request.theme.send_closing_html()

    def getLogin(self, request):
        """
        return the HTML content that presents login options and form
        """

        # describe choices
        content = html.DIV()

        content.append(html.P().append(html.Text("""
Welcome to the NDS Wiki.  You have two options for logging in:
""")))

        fedlogin=request.url + '&login=1'
        c1 = html.A(href=fedlogin)
        c1.append("Login through your home institution (via CI-Logon)")

        c2 = html.A(href="#")
        c2.attrs['onclick'] = "return showlocallogin();"

        choices = \
            html.UL().append(html.LI().append(c1)).append(html.LI().append(c2))
        content.append(

        form = self.getLoginForm(request)

        # integrate form
        return form

    def getLoginForm(self, request):
        """
        return the actual form section
        """

        return loginform.getLogin(request)
