# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - create account action

    @copyright: 2007 MoinMoin:JohannesBerg
    @license: GNU GPL, see COPYING for details.
"""
import re
from MoinMoin import user, wikiutil
from MoinMoin.Page import Page
from MoinMoin.widget import html
from MoinMoin.security.textcha import TextCha
from MoinMoin.auth import MoinAuth
from MoinMoin import log
logging = log.getLogger(__name__)

from ndsorg.moin.util import DN

# set some user default attributes.  
nds_user_attrs = { 'firstname': None,
                   'midname': '',
                   'lastname': None,
                   'institute': None,
                   'country': None,
                   'authid': None
                   }

class NDSUser(user.User):

    def __init__(self, request, id=None, name="", password=None, 
                 auth_username="", **kw):
        self.__dict__.update
        user.User.__init__(self, request, id, name, password,auth_username,**kw)
        
import pdb
def _create_user(request):
    _ = request.getText
    form = request.form

    # validate inputs

    if request.method != 'POST':
        return None, None

    if not wikiutil.checkTicket(request, form.get('ticket', '')):
        return None, None

    if not NewAccountTextCha(request).check_answer_from_form():
        return _('TextCha: Wrong answer! Go back and try again...'), None


    # Create user profile
    if request.user and request.user.valid:
        # the user is already logged in (e.g. via cilogon)
        theuser = request.user
    else:
        theuser = user.User(request, auth_method="new-user")
    for k,v in nds_user_attrs.iteritems():
        theuser.__dict__.setdefault(k,v) 

    # Require non-empty name
    try:
        theuser.name = form['name']
    except KeyError:
        return _("Empty screen name. Please enter a screen name."), None

    # Don't allow creating users with invalid names
    if not user.isValidName(request, theuser.name):
        return _("""Invalid screen name {{{'%s'}}}.
Name may contain any Unicode alpha numeric character, with optional one
space between words. Group page name is not allowed.""", wiki=True) % wikiutil.escape(theuser.name), None

    # Name required to be unique. Check if name belong to another user.
    if user.getUserId(request, theuser.name):
        return _("This screen name already belongs to somebody else."), None

    # try to get the password and pw repeat
    password = form.get('password1', '')
    password2 = form.get('password2', '')

    # Check if password is given and matches with password repeat
    if not theuser.auth_username:
        if password != password2:
            return _("Passwords don't match!"), None
        if not password:
            return _("Please specify a password!"), None

        pw_checker = request.cfg.password_checker
        if pw_checker:
            pw_error = pw_checker(request, theuser.name, password)
            if pw_error:
                return _("Password not acceptable: %s") % \
                    wikiutil.escape(pw_error), None

        # Encode password
        try:
            theuser.enc_password = user.encodePassword(request.cfg, password)
        except UnicodeError, err:
            # Should never happen
            return "Can't encode password: %s" % wikiutil.escape(str(err)), None

    # try to get the email, for new users it is required
    email = wikiutil.clean_input(form.get('email', ''))
    theuser.email = email.strip()
    if not theuser.email and 'email' not in request.cfg.user_form_remove:
        return _("Please provide a valid email address. "), None

    # Email should be unique - see also MoinMoin/script/accounts/moin_usercheck.py
    if theuser.email and request.cfg.user_email_unique:
        if user.get_by_email_address(request, theuser.email):
            return _("This email already belongs to somebody else."), None

    theuser.firstname = form.get('firstname')
    theuser.midname = form.get('midname')
    theuser.lastname = form.get('lastname')
    theuser.institute = form.get('institute')
    theuser.country = form.get('country')
    theuser.authid = form.get('authid')

    # record the ip address from which this registration was submitted as 
    # part of tracking spam
    theuser.registration_ip = request.remote_addr or 'unknown'

    # initiate the sequence for activating the account.  This will, for 
    # example, alert the user or administrator to confirm activation of the 
    # account. 
    # 
    # this may add/update user data for managing the activation of the 
    # account (e.g. initially setting disabled=1).  By default, it does no
    # updates under the assumption that the account is activated immediately
    # upon creation.
    #
    # This function will, unless the request is rejected, save the user data, 
    # thus creating the account.  If the account is rejected, the returned
    # action will be equal to "abort"
    return _initiate_activation(request, theuser)

def _initiate_activation(request, user):
    _ = request.getText
    msg = _("User account created! You can use this account to login now...")
    action = None

    reviewer = getattr(request.cfg, 'newaccount_review', None)
    if reviewer:
        try:
            msg = reviewer(request, user)
            if msg:
                if not isinstance(msg, str) or isinstance(msg, unicode):
                    raise ValueError("Incorrect return value")
                # the reviewer has rejected this request
                return msg, "abort"
        except Exception, e:
            logging.exception("Bad reviewer function implementation: %s" % e)

    # if an activation reaction has been configured, load it and run it.
    activation_name = getattr(request.cfg, 'newaccount_activation', None)
    if activation_name:
        activation = wikiutil.importPlugin(request.cfg, "activation", 
                                           activation_name)
        # TODO: do we need handle a PluginMissingError exception?

        msg, action = activation(request, user)

    user.save()
    return msg, action

def _parse_authid(dnstr):
    """
    parse the components of a X.509 distinguished name
    """
    dn = DN.from_str(dnstr)
    out = { 'institute', dn.orgName }
    name = dn.commonName.split()
    if re.match(r'[a-zA-Z]+\d+', name[-1]):
       name.pop(-1)
    if len(name) > 2 and name[-2] == 'van':
        out['lastname'] = ' '.join(name[-2:])
        out['firstname'] = name[0]
        if len(name) > 3:
            out['midname'] = ' '.join(name[1:-2])
    elif len(name) > 3 and name[-3] == 'van' and name[-3] == 'der':
        out['lastname'] = ' '.join(name[-3:])
        out['firstname'] = name[0]
        if len(name) > 4:
            out['midname'] = ' '.join(name[1:-3])
    elif len(name) > 1:
        out['firstname'] = name[0]
        out['lastname'] = name[-1]
        if len(name) > 2:
            out['midname'] = ' '.join(name[1:-1])
    elif len(name) > 0:
        out['lastname'] = name[0]

    return out;


def _create_form(request):
    _ = request.getText
    url = request.page.url(request)

    # determine if user has already authenticated externally
    userdata = {}
    authid = request.user.valid and request.user.auth_username
    if authid:
        userdata = _parse_authid(authid)

    ret = html.FORM(action=url)
    ret.append(html.INPUT(type='hidden', name='action', value='newaccount'))

    ticket = wikiutil.createTicket(request)
    ret.append(html.INPUT(type="hidden", name="ticket", value="%s" % ticket))

    if authid:
        re.append(html.INPUT(type="hidden", name="authid", value="%s" % authid))

    lang_attr = request.theme.ui_lang_attr()
    ret.append(html.Raw('<div class="userpref"%s>' % lang_attr))
    tbl = html.TABLE(border="0")
    ret.append(tbl)
    ret.append(html.Raw('</div>'))

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(
                                  html.Text(_("First (Given) Name")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="firstname",
                           value="%s" % userdata.get('firstname','') ))
    cell.append(html.Text(' * ')) # required

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(
                                  html.Text(_("Middle Name or Initials")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="midname",
                           value="%s" % userdata.get('midname','') ))
    cell.append(html.Text(' (optional)'))

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(
                                  html.Text(_("Last (Family) Name")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="lastname",
                           value="%s" % userdata.get('lastname','') ))
    cell.append(html.Text(' * ')) # required

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(html.Text(_("Email")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="email"))
    cell.append(html.Text(' * ')) # required

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(html.Text(_("Screen Name")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="name"))
    cell.append(html.Text(' A name that will identify you as a document author '))

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(
                                  html.Text(_("Home Institution")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="institute",
                           value="%s" % userdata.get('institute','') ))
    cell.append(html.Text(' Name of your institution, project, or organization ')) 

    row = html.TR()
    tbl.append(row)
    row.append(html.TD().append(html.STRONG().append(
                                  html.Text(_("Home Country")))))
    cell = html.TD()
    row.append(cell)
    cell.append(html.INPUT(type="text", size="36", name="country",
                           value="%s" % userdata.get('country','') ))
    cell.append(html.Text(' ')) 

    if not authid:
        row = html.TR()
        tbl.append(row)
        row.append(html.TD().append(html.STRONG().append(
                    html.Text(_("Password")))))
        row.append(html.TD().append(html.INPUT(type="password", size="36",
                                               name="password1")))

        row = html.TR()
        tbl.append(row)
        row.append(html.TD().append(html.STRONG().append(
                    html.Text(_("Password repeat")))))
        row.append(html.TD().append(html.INPUT(type="password", size="36",
                                               name="password2")))

    textcha = NewAccountTextCha(request)
    if not authid and textcha.is_enabled():
        row = html.TR()
        tbl.append(row)
        row.append(html.TD().append(html.STRONG().append(
                                      html.Text(_('TextCha (required)')))))
        td = html.TD()
        if textcha:
            td.append(textcha.render())
        row.append(td)

    row = html.TR()
    tbl.append(row)
    row.append(html.TD())
    td = html.TD()
    row.append(td)
    td.append(html.INPUT(type="submit", name="create",
                         value=_('Create Profile')))

    # wrap it in some text
    wrap = html.DIV()
    wrap.append(html.P().append(html.Text("""
Welcome!
""")))
    wrap.append(ret)

    return unicode(wrap)

def execute(pagename, request):
    found = False
    for auth in request.cfg.auth:
        if isinstance(auth, MoinAuth):
            found = True
            break

    if not found:
        # we will not have linked, so forbid access
        request.makeForbidden(403, 'No MoinAuth in auth list')
        return

    page = Page(request, pagename)
    _ = request.getText
    form = request.form

    submitted = form.has_key('create')

    if submitted: # user pressed create button
        msg, reaction = _create_user(request)
        request.theme.add_msg(msg, "dialog")
        if reaction == 'abort':
            reaction = None
        if reaction:
            redirect_url = page.url(request, querystr={'action': reaction})
            return request.http_redirect(redirect_url, code=302)

        return page.send_page()

    else: # show create form
        request.theme.send_title(_("Create Account"), pagename=pagename)

        request.write(request.formatter.startContent("content"))

        # THIS IS A BIG HACK. IT NEEDS TO BE CLEANED UP
        request.write(_create_form(request))

        request.write(request.formatter.endContent())

        request.theme.send_footer(pagename)
        request.theme.send_closing_html()

class NewAccountTextCha(TextCha):

    def __init__(self, request, question=None):
        if not request.cfg.secrets.has_key('security/textcha') and \
           len(request.cfg.secrets) > 0:
            request.cfg.secrets['security/textcha'] = \
                request.cfg.secrets[request.cfg.secrets.keys()[0]]
        TextCha.__init__(self, request, question)

    def _get_textchas(self):
        """ 
        get textchas from the wiki config for the user's language (or 
        default_language or en).  This specialization will get its questions
        preferentially from the newaccount_textchas parameter if it exists;
        otherwise, the textchas paramter will be consulted.  If a textcha 
        should only be used while creating an account, setnewaccount_textchas
        only.
        """
        request = self.request
        cfg = request.cfg
        
        if hasattr(cfg, 'newaccount_textchas'):
            return self._get_newaccount_textchas()
        elif hasattr(cfg, 'textchas'):
            return TextCha._get_textchas(self)

        return None

    def _get_newaccount_textchas(self):
        request = self.request
        groups = request.groups
        cfg = request.cfg
        user = request.user
        textchas = cfg.newaccount_textchas
        if textchas:
            lang = user.language or request.lang
            logging.debug(u"NewAccountTextCha: user.language == '%s'." % lang)
            if lang not in textchas:
                lang = cfg.language_default
                logging.debug(u"NewAccountTextCha: fallback to language_default == '%s'." % lang)
                if lang not in textchas:
                    logging.error(u"NewAccountTextCha: The textchas do not have content for language_default == '%s'! Falling back to English." % lang)
                    lang = 'en'
                    if lang not in textchas:
                        logging.error(u"NewAccountTextCha: The textchas do not have content for 'en', auto-disabling textchas!")
                        cfg.textchas = None
                        lang = None
        else:
            lang = None
        if lang is None:
            return None
        else:
            logging.debug(u"TextCha: using lang = '%s'" % lang)
            return textchas[lang]

        
