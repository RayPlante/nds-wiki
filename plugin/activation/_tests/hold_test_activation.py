# -*- coding: iso-8859-1 -*-

"""
This tests the common activation functions found in activation/__init__.py

@copyright: 2015 MoinMoin:RayPlante
@license: GPL, see COPYING for details
"""
import os
import py.test
from datetime import datetime, timedelta
from cStringIO import StringIO
import activation as act

from _tests.maketestwiki import create_test_request, create_test_user
from MoinMoin.user import User
from MoinMoin.wikiutil import PluginError

class TestUserToken(object):

    def setup_method(self, method):
        self.request = create_test_request(config=None)

    def test_request(self):
        assert self.request.cfg

    def test_create_token(self):
        t1 = act.create_token()
        assert t1
        t2 = act.create_token()
        assert t2
        assert t1 != t2

    def test_missing_user(self):
        token = "gabbagabbahey"

        assert self.request.cfg.cache
        assert act.token2id_via_cache(token, self.request.cfg.cache) is None

        user = act.get_user_by_token(self.request,token,self.request.cfg.cache)
        assert user is None

    def test_user(self):
        cfg = self.request.cfg
        user = create_test_user(self.request)
        token = user.token

        try:
            # user is not yet in the cache
            assert act.token2id_via_cache(token, cfg.cache) is None

            # look up user slow way
            u = act.get_user_by_token(self.request, token)
            assert u and u.id == user.id 
            assert u.token == token

            # look up user slow way, should load cache
            u = act.get_user_by_token(self.request, token, cfg.cache)
            assert u and u.id == user.id 
            assert u.token == token

            # cache should now be loaded
            uid = act.token2id_via_cache(token, self.request.cfg.cache)
            assert uid == user.id 

            # check automatic update of cache
            user.remove()
            assert not user.exists()
            uid = act.token2id_via_cache(token, self.request.cfg.cache)
            assert uid
            u = act.get_user_by_token(self.request, token, cfg.cache)
            assert u is None
            uid = act.token2id_via_cache(token, self.request.cfg.cache)
            assert not uid

        finally:
            if user.exists():
                user.remove()

    def test_cache(self):
        cache = self.request.cfg.cache
        assert cache

        t1 = act.create_token()
        t2 = act.create_token()
        assert act.token2id_via_cache(t1, cache) is None
        assert act.token2id_via_cache(t2, cache) is None
        act.cache_token2id(cache, t1, "uid1")
        act.cache_token2id(cache, t2, "uid2")
        assert act.token2id_via_cache(t1, cache) == "uid1"
        assert act.token2id_via_cache(t2, cache) == "uid2"

        act.cache_del_token(cache, t1)
        assert act.token2id_via_cache(t1, cache) is None
        assert act.token2id_via_cache(t2, cache) == "uid2"

        act.cache_token2id(cache, t1, "uid1")
        assert act.token2id_via_cache(t1, cache) == "uid1"
        act.purge_token2id_cache(cache)
        assert act.token2id_via_cache(t1, cache) is None
        assert act.token2id_via_cache(t2, cache) is None

    def test_time_format(self):
        dt = datetime(2015, 3, 14, 9, 26, 53)
        assert dt.strftime(act.DATETIME_FMT) == "2015-03-14T09:26:53Z"

    def test_token_expires_str(self):
        now = datetime.utcnow().strftime(act.DATETIME_FMT)
        dt = act.token_expires_str(0.025)
        # import pdb; pdb.set_trace()
        if "T00:" not in dt:
            # this test can fail around midnight UTC
            assert now[:11] == dt[:11]
            assert now[11:] != dt[11:]

        diff = datetime.strptime(dt, act.DATETIME_FMT) - \
            datetime.strptime(now, act.DATETIME_FMT) 
        assert diff.days == 0
        assert diff.seconds - 0.025*24*3600 < 1

    def test_set_user_token(self):
        user = create_test_user(self.request)
        assert user.token
        try:
            token = act.set_user_token(self.request, user)
            assert token
            assert hasattr(user, 'token')
            assert user.token == token
            assert user.token_expires
            assert datetime.strptime(user.token_expires, act.DATETIME_FMT) > \
                datetime.now()
        finally:
            user.remove()

    def test_token_expired(self):
        user = create_test_user(self.request)
        try:
            act.set_user_token(self.request, user)
            assert not act.token_expired(user)
            act.set_user_token(self.request, user, -1)
            assert act.token_expired(user)
        finally:
            user.remove()

    def test_needs_activation(self):
        user = create_test_user(self.request)
        try:
            act.set_user_token(self.request, user)
            assert act.needs_activation(user)
            user.disabled = 0
            assert not act.needs_activation(user)
            user.disabled = 1
            assert not act.needs_activation(user)
            user.disabled = act.AWAITING_ACTIVATION

            # test against an expired token
            user.token_expires = \
                (datetime.utcnow() - timedelta(1)).strftime(act.DATETIME_FMT)
            assert not act.needs_activation(user)
        finally:
            user.remove()

    def test_purge_expired_users(self):
        user1, user2 = None, None
        try:
            user1 = create_test_user(self.request, token="__tk__", 
                                     activated=False, firstname="Gurn", 
                                     midname="", lastname="Cranston", 
                                     institute="UIUC", country="US")
            user2 = create_test_user(self.request, token="__tk__", 
                                     activated=False, firstname="Glib", 
                                     midname="", lastname="Parker", 
                                     institute="UIUC", country="US")

            act.set_user_token(self.request, user2)
            act.set_user_token(self.request, user1, -1)
            assert user1.exists()
            assert user2.exists()
            assert not act.needs_activation(user1)
            assert act.needs_activation(user2)

            assert act.purge_expired_users(self.request, [user1, user2]) == 1
            assert not user1.exists()
            assert user2.exists()
            
        finally:
            if user1 and user1.exists(): user1.remove()
            if user2 and user2.exists(): user2.remove()

    def test_disabled(self):
        user = create_test_user(self.request)
        try:
            assert not act.disabled(user)
            user.disabled=1
            assert act.disabled(user)
        finally:
            user.remove()

    def test_needs_activation(self):
        user = create_test_user(self.request)
        try:
            assert not act.needs_activation(user)
            user.disabled=0
            assert not act.needs_activation(user)
            user.disabled=act.AWAITING_ACTIVATION
            assert act.needs_activation(user)

        finally:
            user.remove()

    def test_activate_user(self):
        user = create_test_user(self.request)
        try: 
            act.set_user_token(self.request, user, 2)
            assert act.needs_activation(user)
            assert not hasattr(user, 'activated_by') or not user.activated_by
            act.activate_user(self.request, user)
            assert not act.needs_activation(user)
            assert user.name in user.activated_by
            assert user.activated.startswith(datetime.utcnow().strftime(act.DATETIME_FMT)[:8])
            # check for a disk save
            user = User(self.request, user.id)
            assert user.name in user.activated_by
        finally:
            user.remove()

    def test_cancel_user(self):
        user = create_test_user(self.request)
        try: 
            act.set_user_token(self.request, user, 2)
            assert user.exists()
            assert act.needs_activation(user)
            act.cancel_user(self.request, user)
            assert not user.exists()
        finally:
            if user.exists(): user.remove()

        user = create_test_user(self.request)
        try: 
            user.disabled = 0
            assert user.exists()
            assert not act.needs_activation(user)
            try:
                act.cancel_user(self.request, user)
                py.test.fail()
            except RuntimeError:
                assert user.exists()
            act.cancel_user(self.request, user, force=True)
            assert not user.exists()
            
        finally:
            if user.exists(): user.remove()

        user = create_test_user(self.request)
        try: 
            user.disabled = 1
            assert user.exists()
            assert not act.needs_activation(user)
            try:
                act.cancel_user(self.request, user)
                py.test.fail()
            except RuntimeError:
                assert user.exists()
            act.cancel_user(self.request, user, force=True)
            assert not user.exists()
        finally:
            if user.exists(): user.remove()


class patch(object):
    def __init__(self, monkeypatch): 
        self.mail = None
        import MoinMoin.mail.sendmail
        monkeypatch.setattr(act, 'sendmail', self.sendmail)
        
    def sendmail(self, request, to, subject, text, efrom):
        out = StringIO()
        print >> out, "From:", efrom
        print >> out, "To:", to
        print >> out, "Subject:", subject
        print >> out
        print >> out, text
        self.mail = out.getvalue()
        return True, ""


class TestTemplates:

    def setup_method(self, method):
        self.request = create_test_request(config=None)

    def test_make_activate_url(self):
        url = act.make_activate_url("http://goober.edu/wiki", "WikiHome", 
                                    "gabbagabbahey")
        assert url == "http://goober.edu/wiki/WikiHome?action=activate&token=gabbagabbahey"

    def test_find_template_in_dir(self):
        activdir = os.path.dirname(act.__file__)
        assert act._find_template_in_dir(activdir, "whodowhodo") is None

        # import pdb; pdb.set_trace()
        assert act._find_template_in_dir(activdir, "approve") \
            == os.path.join(activdir, "data", "approve.txt")

    def test_find_template_file(self):
        tmpl = act._find_template_file("approve", self.request.cfg)
        assert tmpl is None
        tmpl = act._find_template_file("approve",self.request.cfg,act.__file__)

    def test_get_email_template(self):
        try:
            subj, tmpl = act.get_email_template("approve", self.request.cfg)
            py.test.fail("Failed to fail on unfound template")
        except PluginError:
            pass

        subj, tmpl = act.get_email_template("approve", self.request.cfg,
                                            act.__file__)
        assert subj and tmpl 
        assert subj.startswith("%(sitename)s Wiki")
        assert tmpl.startswith("Dear ")

    def test_get_template_data(self):
        self.request.cfg.activation_verify_expires_days = 5
        data = act.get_template_data(self.request, goober="Gurn")
        assert data["page_url"]
        assert data["baseurl"]
        assert data["sitename"]
        # assert data["From"]
        assert data["goober"] == "Gurn"
        assert data["token_lifetime"] == "5 days"

        assert not data.has_key("activation_verify_expires_days")

        self.request.cfg.activation_verify_expires_days = 0.05
        user = create_test_user(self.request)
        try: 
            data = act.get_template_data(self.request, user)
            assert data["page_url"]
            assert data["baseurl"]
            assert data["sitename"]
            # assert data["From"]

            assert not data.has_key("goober")
            assert not data.has_key("activation_verify_expires_days")

            assert data["user.name"] == "__TestUser__"
            assert data["user.language"] == ""
            assert not data.has_key("email")
            assert data["token_lifetime"] == "1 hour"
        finally:
            user.remove()

    def test_apply_template_data(self):
        user = create_test_user(self.request)
        try: 
            data = act.get_template_data(self.request, user, 
                                         activate_page="ApproveNewUser", 
                                         token="__token__", From="adm@mo.in")

            assert data["From"] == "adm@mo.in"
            assert data["token"] == "__token__"
            assert data["activate_page"] == "ApproveNewUser"
            assert data["activate_url"] == \
              "http://localhost//ApproveNewUser?action=activate&token=__token__"

            subj, tmpl = act.get_email_template("approve", self.request.cfg, 
                                                act.__file__)
            subj %= data
            assert '%(' not in subj
            tmpl %= data
            assert '%(' not in tmpl

        finally:
            user.remove()

    def test_send_activate_email(self, monkeypatch):
        p = patch(monkeypatch)

        body = """
Dear %(user.name)s:  Please login into the %(sitename)s Wiki.
"""
        self.request.cfg.mail_from = u"wiki@ex.org"
        user = create_test_user(self.request)
        try: 
            act.send_activate_email("user@ex.org", "activate %(user.name)s", 
                                    body, self.request, user)

            assert p.mail
            assert p.mail.startswith("From:")
            assert "%(" not in p.mail
            assert "Please" in p.mail
            assert "Subject: activate" in p.mail
        finally:
            user.remove()


        
