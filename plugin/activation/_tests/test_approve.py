# -*- coding: iso-8859-1 -*-

"""
This tests the approve activation plugin

@copyright: 2015 MoinMoin:RayPlante
@license: GPL, see COPYING for details
"""
import os
import pytest
from cStringIO import StringIO

from _tests.maketestwiki import create_test_request, create_test_user
from MoinMoin.user import User
from MoinMoin import wikiutil

class patch(object):
    def __init__(self, monkeypatch, request): 
        self.mail = None
        activation = import_activation(request)
        monkeypatch.setattr(activation, 'sendmail', self.sendmail)
        
    def sendmail(self, request, to, subject, text, efrom):
        out = StringIO()
        print >> out, "From:", efrom
        print >> out, "To:", to
        print >> out, "Subject:", subject
        print >> out
        print >> out, text
        self.mail = out.getvalue()
        return True, ""

# we have to import the macro as a plugin
def import_approve(request):
    cfg = request.cfg
    assert len(request.cfg._plugin_modules) > 0

    approvef = \
       wikiutil.importWikiPlugin(cfg,"activation","approve")

    return approvef

def import_activation(request):
    activation = wikiutil.importPlugin(request.cfg, "activation", 
                                       "common", "import_module")(request.cfg)
    assert activation
    return activation

def test_import():
    request = create_test_request()
    appf = import_approve(request)
    assert appf.__name__ == 'execute'

    request = create_test_request()
    activation = import_activation(request)
    assert activation.get_email_template

class TestApprove(object):

    def setup_method(self, method):
        self.request = create_test_request()
        self.user = create_test_user(self.request, activated=False)

    def teardown_method(self, method):
        self.user.remove()
        del self.user

    def get_activation_function(self, name=None):
        if name:
            return wikiutil.importWikiPlugin(self.request.cfg, "activation", 
                                             "approve", name)

        # return execute()
        return wikiutil.importWikiPlugin(self.request.cfg, "activation", 
                                         "approve")

    def test_get_approve_address(self):
        func = self.get_activation_function('get_approve_address')
        addr = func(self.request, self.user)
        assert addr == u'Wiki Admin <adm@mo.in>' # mail_from

        setattr(self.request.cfg, 'activation_approve_addr', u'approve@mo.in')
        addr = func(self.request, self.user)
        assert addr == u'approve@mo.in'

        def addrf(request, user):
            return "me@mo.in"

        setattr(self.request.cfg, 'activation_get_approve_addr', addrf)
        addr = func(self.request, self.user)
        assert addr == u'me@mo.in'

    def test_request_approval(self, monkeypatch):
        p = patch(monkeypatch, self.request)
        setattr(self.request.cfg, 'activation_approve_page', 'AccountApproval')

        # pytest.set_trace()
        func = self.get_activation_function('request_approval')
        msg, nextact = func(self.request, self.user)

        assert msg == ''
        assert nextact is None
        assert "Registrar" in p.mail
        assert "AccountApproval" in p.mail
        assert "%(" not in p.mail

    def test_execute(self, monkeypatch):
        p = patch(monkeypatch, self.request)
        setattr(self.request.cfg, 'activation_approve_page', 'AccountApproval')

        # pytest.set_trace()
        func = self.get_activation_function('execute')
        msg, nextact = func(self.request, self.user)

        assert msg
        assert nextact is None
        assert 'await' in msg 
        assert "Registrar" in p.mail
        assert "AccountApproval" in p.mail
        assert "%(" not in p.mail

