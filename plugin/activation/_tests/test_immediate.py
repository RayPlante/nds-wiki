# -*- coding: iso-8859-1 -*-

"""
This tests the immediate activation plugin

@copyright: 2015 MoinMoin:RayPlante
@license: GPL, see COPYING for details
"""
import os
import py.test

from _tests.maketestwiki import create_test_request, create_test_user
from MoinMoin.user import User
from MoinMoin import wikiutil

# we have to import the macro as a plugin
def import_immediate(request):
    cfg = request.cfg
    assert len(request.cfg._plugin_modules) > 0

    activatef = wikiutil.importWikiPlugin(cfg,"activation","immediate")
    return activatef

def import_activation(request):
    activation = wikiutil.importPlugin(request.cfg, "activation", 
                                       "common", "import_module")(request.cfg)
    assert activation
    return activation

def test_import():
    request = create_test_request()
    immf = import_immediate(request)
    assert immf.__name__ == 'execute'

    activation = import_activation(request)
    assert activation.get_email_template

class TestImmediate(object):

    def setup_method(self, method):
        self.request = create_test_request()
        self.user = create_test_user(self.request)

    def teardown_method(self, method):
        self.user.remove()
        del self.user

    def get_activation_function(self, name=None):
        if name:
            return wikiutil.importWikiPlugin(self.request.cfg, "activation", 
                                             "immediate", name)

        # return execute()
        return wikiutil.importWikiPlugin(self.request.cfg, "activation", 
                                         "immediate")

    def test_execute(self):
        self.user.disabled=2
        func = self.get_activation_function()

        msg, nextact = func(self.request, self.user)
        assert self.user.disabled == 0
        assert msg
        assert "created" in msg
        assert nextact is None
