"""
    MoinMoin - activation plugin: request that a designated administrator
               approve the account.
    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

    This activation plugin will send an email to a configured administrator
    requesting to approve the account.  By default, this will be the 
    account given in the 'mail_from' configuration item; however, a special
    address can be give via 'activation_approve_addr'.  To highly 
    customize based on the user information, create a function within the 
    configuration class called 'activation_get_approve_addr' that 
    takes the request and user objects as arguments and returns the email 
    address.  If None is returned, the default administrator's address will
    be used.

    class LocalConfig(multiconfig.DefaultConfig):
        # Directory containing THIS wikiconfig:
        wikiconfig_dir = os.path.abspath(os.path.dirname(__file__))
        #
        # :
        # : other stuff defined in wikiconfig.py that doesn't concern us
        # :
        #
        # ------------ Lines below are the ones added -----------
        def activation_get_approve_addr(request, user):
            if user.email.endswith("@my-company.com"):
               admin = "Wiki Support <wiki@my-company.com>"
            else:
               admin = None
            return admin

"""
import time
from datetime import datetime
from MoinMoin.wikiutil import importNameFromPlugin, PluginMissingError
from MoinMoin import log
logging = log.getLogger(__name__)

# If the activation plugin is ever integrated into the mainline MoinMoin,
# replace the next line with:
#
#   from MoinMoin import activation
#
activation = None

DEFAULT_EADDR_CFGITEM = 'mail_from'
APPROVE_EADDR_CFGITEM = 'activation_approve_addr'
EADDR_FUNC_CFGITEM = 'activation_get_approve_addr'
APPROVE_EMAIL_TMPL_CFGITEM = 'activation_approve_email_template'
APPROVED_EMAIL_TMPL_CFGITEM = 'activation_approved_email_template'
NOAPPROVED_EMAIL_TMPL_CFGITEM = 'activation_disapprove_email_template'
APPROVE_PAGE_CFGITEM = 'activation_approve_page'

#
# Activation process entry point.
#
def execute(request, user):
    """
    request activation of account by an appropriate administrator
    """
    _ = request.getText
    msg, nextact = request_approval(request, user)
    if not msg:
        msg = _("Account activation requested; please await an email confirming activation before logging in.")

    return msg, nextact

def request_approval(request, user):
    """
    request activation of account by an appropriate administrator
    """
    _import_activation_module(request)
    _ = request.getText
    addr = get_approve_address(request, user)
    
    tmplname = getattr(request.cfg, APPROVE_EMAIL_TMPL_CFGITEM, None)
    if not tmplname:
        tmplname = "approve"

    defsubj = _("%(sitename)s Wiki Account Approval Requested")
    subj, body = activation.get_email_template(tmplname, request.cfg, __file__)
    if not subj:
        subj = defsubj

    user.activation = "approve"
    activation.set_user_token(request, user)

    activate_page = getattr(request.cfg, APPROVE_PAGE_CFGITEM, None)
    if not activate_page:
        activate_page = request.cfg.page_front_page

    if not activation.send_activate_email(addr, subj, body, request, user,
                                          activate_page=activate_page):
        msg = _("Failed to send activation email to an administrator; " +
                "try again later or contact the wiki administrator.")
        return msg, "abort"

    return '', None

def get_approve_address(request, user):
    """
    retrieve the configured address for the administrator that can 
    approve this account.  At a minimum, the mail_from configuration
    item should be set.  
    """
    addr = None
    get_addr = getattr(request.cfg, EADDR_FUNC_CFGITEM, None)
    if get_addr:
        addr = get_addr(request, user)

    if not addr:
        addr = getattr(request.cfg, APPROVE_EADDR_CFGITEM, None)
    if not addr:
        addr = getattr(request.cfg, DEFAULT_EADDR_CFGITEM, None)

    return addr

def on_activate(request, user, result):
    """
    this will get called when the account is activated.
    """
    _import_activation_module(request)

    tmpl = None
    if result == 'activate':
        # email the user to inform that the account is ready for use.  
        tmpl = getattr(request.cfg, APPROVED_EMAIL_TMPL_CFGITEM, None)

    elif result == 'drop':
        # email the user to inform that the account was not approved.
        tmpl = getattr(request.cfg, NOAPPROVED_EMAIL_TMPL_CFGITEM, None)

    if tmpl:
        try:
            subj, body = activation.get_email_template(tmpl, request.cfg)
            activation.send_activate_email(user.email, subj, body, 
                                           request, user)
        except PluginMissingError, e:
            logging.exception(e)


#
# this function is not needed if this macro is made built-in
#    
def _import_activation_module(request):
    global activation
    if not activation:
        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)

