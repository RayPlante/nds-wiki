"""
    MoinMoin - common activation functions
    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

    This activation module contains functions that can be used commonly
    from other activation plugins.  To import a particular function 
    (e.g. needs_activation()), use the following:

        from MoinMoin.wikiutil import importPlugin
        needs_activation = importPlugin(request.cfg, "activation", 
                                        "common", "needs_activation")

    To import the entire module of common functions, use this:

        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)

"""
import re, os, hashlib, random, math
from datetime import datetime, timedelta

from MoinMoin.util import pysupport
from MoinMoin import wikiutil
from MoinMoin.mail.sendmail import sendmail
from MoinMoin import log
import MoinMoin.user as userutil
logging = log.getLogger(__name__)

AWAITING_ACTIVATION=2  # user.disabled value that indicates awaiting activation
TEMPLATE_DIR = "data"
TOKEN_EXPIRES_CFGITEM = 'activation_verify_expires_days'

def import_module(cfg):
    """
    return a reference to this common module.  

    When the activation framework is available only as a plugin (and not 
    part of the MoinMoin core), normal use of import will not be helpful.  
    To import this module, use this instead:

        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)

    """
    moditem = wikiutil.importPlugin(cfg, "activation", 
                                    "common", "import_module")
    assert moditem
    actmodulename = moditem.__module__.rsplit('.', 1)
    actmodule = __import__(actmodulename[0], globals(), {}, actmodulename[1])
    return getattr(actmodule, actmodulename[1])

def get_email_template(tmplname, cfg=None, plugin_file=None):
    """
    find and return the template with the given name.  

    @param tmplname:   the name of the template to use as the message.  
                 This should correspond to a filename under the data directory
                 below the activation plugin directory.  This file will be 
                 looked for first under wiki/data/plugin/activation/data and 
                 then under MoinMoin/activation/data.
    @param cfg:  a configuration object that indicates the location of the 
                 wiki directory.  If None, the wiki plugin directories will not 
                 be checked.
    @param plugin_file:  The value of __file__ for the calling activation 
                 module.  If this is not a built-in activation (i.e. it is 
                 a plugin), it's directory will be searched first for the 
                 template.  If None, it will not be preferentially searched.
    """
    tmplfile = _find_template_file(tmplname, cfg, plugin_file)
    if not tmplfile:
        raise wikiutil.PluginMissingError("Activation email template not found: " + tmplname)

    hdrre = re.compile("^(Subject):\s*(\S.*)$")
    hdr = {"Subject": ''}
    with open(tmplfile) as tmplfd:
        line = tmplfd.readline()
        while line is not None:
            hdrmatch = hdrre.match(line)
            if not hdrmatch:
                while line is not None and len(line.strip()) == 0:
                    line = tmplfd.readline()
                break
            hdr[hdrmatch.group(1)] = hdrmatch.group(2)
            line = tmplfd.readline()

        body = tmplfd.read()
        if line:
            body = "%s\n%s" % (line, body)

    return hdr["Subject"], body

def _find_template_file(tmplname, cfg, plugin_file=None):
    """
    find the file containing the named activation email template and 
    return its full path.  

    @param tmplname:   the name of the template to use as the message.  
                 This should correspond to a filename under the data directory
                 below the activation plugin directory.  This file will be 
                 looked for first under wiki/data/plugin/activation/data and 
                 then under MoinMoin/activation/data.
    @param cfg:  a configuration object that indicates the location of the 
                 wiki directory.  If None, the wiki directory will not be 
                 searched.
    @param plugin_file:  The value of __file__ for the calling activation 
                 module.  If this is not a built-in activation (i.e. it is 
                 a plugin), it's directory will be searched first for the 
                 template.  If None, it will not be preferentially searched.
    """
    path = None

    if plugin_file:
        activdir = os.path.dirname(plugin_file)
        if os.path.basename(os.path.dirname(activdir)) != "MoinMoin":
            # this is from a plugin directory; search here first
            path = _find_template_in_dir(activdir, tmplname)

    # search activation plugin directories.  
    if not path and cfg:
        # this mimics MoinMoin.wikiutil.wikiPlugins()
        for modname in cfg._plugin_modules:
            try:
                # raises exception if activation package not found here
                module = pysupport.importName(modname, "activation")
                activdir = os.path.dirname(module.__file__)
                path = _find_template_in_dir(activdir, tmplname)
                if path:
                    break
            except (AttributeError, ImportError):
                pass

    # search builtin activation area
    if not path:
        activdir = os.path.dirname(__file__)
        if os.path.basename(os.path.dirname(activdir)) == "MoinMoin":
            # this is only true if this code has been integrated into 
            # the mainline MoinMoin code
            path = _find_template_in_dir(activdir, tmplname)

    return path

def _find_template_in_dir(activdir, tmplname):
    tmplpath = os.path.join(activdir, TEMPLATE_DIR, tmplname+".txt")
    if os.path.exists(tmplpath):
        return tmplpath
    return None

def send_activate_email(to, subject, body, request, user, **kw):
    """
    send an activation request email to the address given by <to> using 
    the template named <tmplname>.  

    @param to:      the email address to send the message to.  This can be 
                    a raw email address or formatted in the form of 
                    "Fullname <email_address>"
    @param subject: the subject template for the email message.  This can be 
                    over-ridden by the template itself.  
    @param body:    the template text to use as the message.  
                    This should correspond to a filename under the data 
                    directory below the activation plugin directory.  This 
                    file will be looked for first under 
                    wiki/data/plugin/activation/data and then under 
                    MoinMoin/activation/data.
    @param request: the HTTP request object that submitted the account 
                    creation (or activation) request
    @param user:    the User object for the account to be activated
    @param kw:      other parameters to provide for the templates
    @rtype: bool
    @return: True if the email was sent successfully.  If False, an error
        message will appear in the log
    """
    _ = request.getText

    efrom = request.cfg.mail_from
    if not efrom:
        logging.error("no mail_from parameter value set; Please set in config")
        raise RuntimeError("no mail_from parameter value set")

    data = get_template_data(request, user, From=efrom, escape=False, **kw)

    subject = subject % data
    body = body % data

    mailok, msg = sendmail(request, to, subject, body, efrom)
    if not mailok:
        logging.error("Failed to send activation email: "+msg)

    return bool(mailok)

def get_template_data(request, user=None, escape=True, **kw):
    """
    return a dictionary of data that can be applied to email message 
    templates.

    @param request: the HTTP request object that submitted the account 
                    creation (or activation) request
    @param user:    the User object for the account to be activated
    @param escape:  If True (default), values will be escaped to allow 
                    safe inclusion in a web page.  Set to false if this 
                    is used to create a plain-text email message.  Set 
                    this to a function to provide a specialized escape 
                    function.  
    @param *kw:     arbitrary keywords to include in the output dictionary.
                    these will override any extracted from request and 
                    user.  
    """
    data = {}
    cfg = request.cfg

    if not hasattr(escape, '__call__'):
        noescape = lambda s: s
        escape = (escape and wikiutil.escape) or noescape

    # data from the request state
    data["page_url"] = request.page.url(request)
    data["baseurl"] = request.url_root
    data["home_url"] = "/".join([request.url_root, request.cfg.page_front_page])

    # data from the configuration
    data["sitename"] = cfg.sitename
    if hasattr(cfg, "mail_from"):
        data["From"] = cfg.mail_from
    if hasattr(cfg, TOKEN_EXPIRES_CFGITEM):
        lifetime = get_token_lifetime(request, user)
        data["token_expires_days"] = lifetime
        unit = "day"
        if lifetime < 1:
            lifetime *= 24
            unit = "hour"
        if lifetime < 1:
            lifetime *= 60
            unit = "minute"
        if (unit != 'minute' or lifetime < 1):
            lifetime = int(math.floor(lifetime))
        if (lifetime != 1): unit += 's'
        data["token_lifetime"] = "%s %s" % (lifetime, unit)
    else:
        data["token_lifetime"] = "a few days"
    if hasattr(cfg, "activation_approve_page"):
        data["activation_approve_page"] = cfg.activation_approve_page
    if hasattr(cfg, "activation_verify_page"):
        data["activation_verify_page"] = cfg.activation_verify_page

    # user data
    if user:
        useritems = "aliasname name email firstname midname lastname".split()
        useritems.extend("language real_language institute country".split())
        useritems.extend("approved_by token token_expires id".split())
        for item in useritems:
            if hasattr(user, item):
                data["user."+item] = escape(getattr(user, item))

    # override with given arbitrary data
    for key in kw:
        data[key] = wikiutil.escape(kw[key])

    if data.has_key("user.token") and not data.has_key("token"):
        data['token'] = data['user.token']

    if data.has_key("baseurl") and data.has_key("activate_page"):
        data['activate_form'] = make_activate_url(data["baseurl"], 
                                                  data["activate_page"])
        if data.has_key("token"):
            data['activate_url'] = \
                make_activate_url(data["baseurl"], data["activate_page"], 
                                  data["token"])

    return data

def make_activate_url(baseurl, page, token=None):
    """
    return an account activation URL from a given token
    """
    out = "%s/%s?action=activate" % (baseurl, page)
    if token:
        out += "&token=%s" % token
    return out

def create_token():
    """
    create a random string to be used as an authorization token
    """
    return hashlib.md5(str(random.getrandbits(128))).hexdigest()

def set_user_token(request, user, lifetime=None):
    """
    set the user account's status to "awaiting activation" and set a token
    to 
    @param request:   the request object currently in use
    @param user:      the user that needs a token
    @param lifetime:  the lifetime of the token.  If None, the lifetime
                        is taken from the configuration
    """
    token = create_token()
    user.token = token
    user.disabled=AWAITING_ACTIVATION  # 2 

    if not lifetime:
        lifetime = get_token_lifetime(request, user)
        
    user.token_expires = token_expires_str(lifetime)

    return token

def get_token_lifetime(request, user, default=2):
    try:
        lifetime = getattr(request.cfg, TOKEN_EXPIRES_CFGITEM, default)

        # allow this item to be a function
        if hasattr(lifetime,'__call__'):
            if not user:
                return default
            try:
                lifetime = lifetime(request, user)
            except Exception, e:
                logging.exception("Misconfigured %s function (defaulting to 2): %s" % 
                                  (TOKEN_EXPIRES_CFGITEM, e))
                raise
        return float(lifetime)
    except:
        return default


DATETIME_FMT = '%Y-%m-%dT%H:%M:%SZ'

def token_expires_str(lifetime):
    """
    return a formatted string indicating the expiration of a token with 
    a given lifetime from now.

    @param lifetime:  the token's lifetime in days
    """
    return (datetime.utcnow() + timedelta(lifetime)).strftime(DATETIME_FMT)

def disabled(user):
    """
    return true if this user exists but has been explicitly disabled.
    A disabled account prevents the user from logging in.
    """
    return user.disabled == 1 and user.exists() 

def needs_activation(user):
    """
    return True if this user account is awaiting activation.  A user whose
    token has expired will return False.  
    """
    return user.disabled == AWAITING_ACTIVATION and not token_expired(user)

def token_expired(user):
    """
    return True if this user's activation token has expired.  Users 
    with expired tokens are eligible for removal.
    """
    if hasattr(user, 'token_expires') and user.token_expires:
        try:
            expires = datetime.strptime(user.token_expires, DATETIME_FMT)
            return expires < datetime.utcnow()
        except ValueError:
            logging.warn("Bad format for token_expires for user.id=%s: %s; will purge." %
                         (user.id, user.token_expires))
            return True
        
    return False

def get_user_by_token(request, token, cache=None):
    """
    return the user object for the user issued a given token

    @param request:   the page request object
    @param token:     the issued token
    @param cache:     the optional cache that can be used to store a 
                        look-up of tokens to user IDs.  This will be 
                        updated.  
    """
    if cache:
        uid = token2id_via_cache(token, cache)
        if uid:
            user = userutil.User(request, uid)
            if user.exists():
                return user
            else:
                cache_del_token(cache, token)

    for uid in userutil.getUserList(request):
        user = userutil.User(request, uid)
        if hasattr(user, "token"):
            if cache:
                cache_token2id(cache, user.token, uid)
            if user.token == token:
                return user

    return None

def token2id_via_cache(token, cache):
    """
    consult the given cache to return the user id for the user assigned 
    the give token

    @param token:   the token to look up
    @param cache:   the cache object (gotten from request.cfg.cache) to 
                    consult.  The cache's "token2id" attribute contains 
                    the actual look-up dictionary; if it does exist, None
                    is returned.  
    @rtype: str or None
    @return:  the user identifier associated with the token or None if the 
              token is not currently in the cache.
    """
    if hasattr(cache, "token2id"):
        return cache.token2id.get(token)

def cache_token2id(cache, token, uid):
    """
    associate a token with a user ID in the given cache

    @param cache:   the cache object (gotten from request.cfg.cache) to 
                    update.  The cache's "token2id" attribute contains 
                    the actual look-up dictionary; if it does exist, it 
                    will be created.  
    @param token:   the token to add to the cache
    @param uid      the user ID to associate with the token
    """
    if not hasattr(cache, "token2id"):
        cache.token2id = {}
    cache.token2id[token] = uid

def cache_del_token(cache, token):
    """
    remove the given token's mapping to a user ID from the given cache

    @param cache:   the cache object (gotten from request.cfg.cache) to 
                    update.  The cache's "token2id" attribute contains 
                    the actual look-up dictionary.
    @param token:   the token to remove from the cache
    """
    if hasattr(cache, "token2id") and cache.token2id.has_key(token):
        del cache.token2id[token]

def get_cached_token_uids(cache):
    """
    return the list of cached user ids for users with tokens
    """
    if not cache or not hasattr(cache, "token2id"):
        return []

    return cache.token2id.values()
        
def purge_token2id_cache(cache):
    """
    clear all token-user mappings in the cache
    
    @param cache:   the cache object (gotten from request.cfg.cache) to 
                    purge.  The cache's "token2id" attribute contains 
                    the actual look-up dictionary.
    """
    if hasattr(cache, "token2id"):
        cache.token2id = {}

def purge_expired_users(request, users=None, cacheonly=False):
    """
    examine each of the provided users and remove any who await activation
    but whose tokens have expired.  

    @param request:   the current request object
    @param users:     a list of user objects to examine.  If None, all 
                        existing users in the system will be examined
    @param cacheonly: if users is None, only examine the users in the 
                        token cache
    """
    if users is None:
        if cacheonly:
            users = [ userutil.User(request, id=userid)
                      for userid in get_cached_token_uids(request.cfg.cache) ]
        else:
            logging.debug("Checking all users for expired tokens")
            users = [ userutil.User(request, id=userid)
                      for userid in userutil.getUserList(request) ]
    else:
        logging.debug("Checking selected users for expired tokens")

    purged = []
    for user in users:
        if user.disabled == AWAITING_ACTIVATION and token_expired(user):
            cache_del_token(request.cfg.cache, user.token)
            user.remove()
            purged.append( (user.name, user.id, user.email) ) 

    if len(purged) > 0:
        userutil.clearLookupCaches(request)
        userutil.rebuildLookupCaches(request)

        msg = "Purged %s user%s with expired token:\n\t" % \
            (len(purged), (len(purged) > 1 and "s") or '')
        logging.info(msg + "\n\t".join(["%s (%s) <%s>" % u for u in purged]))
    else:
        logging.debug("No expired tokens found; no users removed.")

    return len(purged)

def activate_user(request, user):
    """
    update the user attributes corresponding to the given user identifier
    so as to enable use of their account.

    @param request:    the current request object
    @param user:       the user object for the account to activate
    @rtype None
    """
    # update the user metadata
    if request.user.valid:
        user.activated_by = "%s <%s>" % (request.user.name, request.user.email)
    else:
        user.activated_by = "%s <%s>" % (user.name, user.email)
    user.activated = datetime.utcnow().strftime(DATETIME_FMT)
    user.disabled = 0

    user.save()

def drop_user(request, user, force=False):
    """
    drop a user's account.  

    @param request:    the current wiki request object
    @param user:       either the identifier for the user account to be 
                       dropped or a MoinMoin.User.user object.
    @param force:      if False (default), the user attributes are checked to
                       ensure that the account is in a pending state; if it 
                       isn't the operation will fail.  If True, the check is 
                       not done and the account is dropped regardless. 
    @rtype None
    """

    if not isinstance(user, userutil.User):
        user = userutil.User(request, user)
    if not user.exists():
        raise RuntimeError("Non-existant user (%s)" % user.id)

    if not force and user.disabled != AWAITING_ACTIVATION:
        raise RuntimeError("Account drop failed: requested user (%s, %s) is not awaiting activation" % (user.name, user.id))

    # Bye-bye
    user.remove()

    # clear the cache
    if request.cfg.cache:
        if hasattr(user, 'token'):
            cache_del_token(request.cfg.cache, user.token)
        try:
            del request.cfg.cache.name2id[user.name]
            del request.cfg.cache.name2id_lower[user.name.lower()]
        except:
            pass
        try:
            del request.cfg.cache.email2id[user.email]
            del request.cfg.cache.email2id_lower[user.email.lower()]
        except:
            pass
        try:
            del request.cfg.cache.jid2id[user.jid]
            del request.cfg.cache.jid2id_lower[user.jid.lower()]
        except:
            pass
        try:
            del request.cfg.cache.openids2id[user.openids]
            del request.cfg.cache.openids2id_lower[user.openids.lower()]
        except:
            pass

