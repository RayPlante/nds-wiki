"""
    MoinMoin - activatation plugin: immediately activate an account upon
               creation
    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

    This is a default implementation of a trivial account activation: do
    nothing special and immediately enable the account.
"""

def execute(request, user):
    """
    execute immediate activation.  This will set disable=0 and return no 
    follow-on action. 
    """
    user.disabled=0
    _ = request.getText
    msg = _("User account created! You can now login as %s...") % user.name

    return msg, None


    
