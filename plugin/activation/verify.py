"""
    MoinMoin - activation plugin: request that a designated administrator
               approve the account.
    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

    This activation plugin will send an email to the user asking them to 
    verify their email address.  The email contains a secret token which 
    the user provides back to a verification page; the account is then 
    activation and is ready for use.  
"""
import time
from datetime import datetime
from MoinMoin.wikiutil import importNameFromPlugin, PluginMissingError
from MoinMoin import log
logging = log.getLogger(__name__)

# If the activation plugin is ever integrated into the mainline MoinMoin,
# replace the next line with:
#
#   from MoinMoin import activation
#
activation = None

VERIFY_EMAIL_TMPL_CFGITEM = 'activation_verify_email_template'
VERIFIED_EMAIL_TMPL_CFGITEM = 'activation_activated_email_template'
DROPPED_EMAIL_TMPL_CFGITEM = 'activation_dropped_email_template'
VERIFY_PAGE_CFGITEM = 'activation_activate_page'

#
# Activation process entry point.
#
def execute(request, user):
    """
    send an email to the user with verification instructions
    """
    _ = request.getText
    msg, nextact = request_verification(request, user)
    if not msg:
        msg = _("Email verification requested; please look for an email with instructions for activating your account.")

    return msg, nextact

def request_verification(request, user):
    """
    send an email to the user with verification instructions
    """
    _import_activation_module(request)
    _ = request.getText

    tmplname = getattr(request.cfg, VERIFY_EMAIL_TMPL_CFGITEM, None)
    if not tmplname:
        tmplname = "verify"

    actpage = getattr(request.cfg, VERIFY_PAGE_CFGITEM, 
                      request.cfg.page_front_page)
    if not actpage:
        msg = "Neither activation_activate_page nor page_front_page are " + \
            "set in configuration!"
        logging.error(msg)
        raise RuntimeError(msg)

    defsubj = _("%(sitename)s Wiki Account Approval Requested")
    subj, body = activation.get_email_template(tmplname, request.cfg, __file__)
    if not subj:
        subj = defsubj

    user.activation = "verify"
    activation.set_user_token(request, user)

    if not activation.send_activate_email(user.email, subj, body, request, user,
                                          activate_page=actpage):
        msg = _("Failed to send your verification email; " +
                "try again later or contact the wiki administrator.")
        return msg, "abort"

    return '', None


def on_activate(request, user, result):
    """
    this will get called when the account is activated.
    """
    _import_activation_module(request)

    tmpl = None
    if result == 'activate':
        # email the user to inform that the account is ready for use.  
        tmpl = getattr(request.cfg, VERIFIED_EMAIL_TMPL_CFGITEM, None)

    elif result == 'drop':
        # email the user to inform that the account was not approved.
        tmpl = getattr(request.cfg, DROPPED_EMAIL_TMPL_CFGITEM, None)

    if tmpl:
        try:
            subj, body = activation.get_email_template(tmpl, request.cfg)
            activation.send_activate_email(user.email, subj, body, 
                                           request, user)
        except PluginMissingError, e:
            logging.exception(e)


#
# this function is not needed if this macro is made built-in
#    
def _import_activation_module(request):
    global activation
    if not activation:
        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)



