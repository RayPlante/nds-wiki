"""
"""
import os, sys

try:
    import MoinMoin
except ImportError:
    try:
        # look for a moinmoin distribution directory to test against; look
        # for it within the nds-wiki distribution directory.  It should have
        # the name moin-VERSION
        distdir = os.path.dirname(os.path.dirname(__file__))
        moindirs = filter(lambda d: d.startswith('moin-'), os.listdir(distdir))
        moindirs.sort()
        
        sys.path.append(os.path.join(distdir, moindirs[-1]))
    except:
        # oh well; fail
        import MoinMoin

