"""
    MoinMoin - account activation macro plugin: creates an interface for 
               approving/activating accounts 
    @copyright: 2015 Raymond L. Plante
    @license: GNU GPL, see COPYING for details.

    This re-uses the design and code from the deprecated emailActivation 1.1.4.

    The macro is responsible for creating the HTML interface used to activate 
    new accounts, either a single account by its user who was sent a 
    verification token by email or multiple accounts by a superuser.  
    Processing the results from these interfaces is handled by the activate
    action.

    This macro is intended to be used in pages that users are directed to
    via emails to activate an account.  (The page names are configured via 
    the activation_approve_page and activation_verify_page configuration 
    items; see activation documentation.)  The page can include arbitrary 
    content plus a single instance of this macro ("<<AccountActivation>>");
    the appropriate interface, depending whether the user is signed in as 
    a super-user or provides a token as a URL argument.
"""
import MoinMoin.user as userutil

# If the activation plugin is ever integrated into the mainline MoinMoin,
# replace the next line with:
#
# from MoinMoin import activation
activation = None

#
# Macro entry point.
#
def execute(macro, args=None):
    # note: this macro does not currently support any parameters

    form = dict(macro.request.args)
    form.update(macro.request.form)
    return activation_interface(macro.request, form)

def activation_interface(request, form):
    """
    display an interface for activating one or more accounts.  If the current
    user is a superuser, all pending accounts will be shown.  If a token is 
    provided, the activation interface for the corresponding account will be
    displayed.

    @param request:  the request object for the page to be returned
    @param form:     the query parameters for the request as a dictionary.
    """
    _ = request.getText
    
    if request.user.isSuperUser() and request.user.valid:
        # show all pending requests
        return super_activation_interface(request, form)

    token = form.get('token', [None])[0]
    if token is not None:
        # show request for specific user
        return user_activation_interface(request, form, token)

    # show an interface that a token can be pasted into
    return activation_request(request)

ACTIVATION_REQUEST_FORM = """
<p>
If you created an account in this wiki, you should have received an email
containing an activation token.  Enter that token in the box below to activate 
(or drop) your account.
</p>
<form action="%(form_action)s" method="POST">

<p> Token: 
<input type="text" name="token" value="" width="32" /> &nbsp;&nbsp;
<input type="submit" name="submit" value="Lookup" />   <br />
</p>
</form>
"""

def activation_request(request):
    """
    return an HTML fragment that will display an interface for pasting in 
    a token.
    """
    faction = request.page.url(request, {"action": "activate"})

    _import_activation_module(request)
    data = activation.get_template_data(request, form_action=faction)
    return ACTIVATION_REQUEST_FORM % data

USER_ACTIVATION_FORM = """
<p>
Select "Activate" to use your account or "Drop" to delete the account.
Select "Cancel" to postpone verification until later.
</p>
<form action="%(form_action)s" method="POST">
<input type="hidden" name="token" value="%(token)s" />
<p> 
<b>Screen Name:</b> %(user.name)s <br />
<b>Full Name:</b> %(user.firstname)s  %(user.midname)s  %(user.lastname)s <br />
<b>Email:</b> %(user.email)s <br />
<input type="submit" name="submit" value="Activate" />
<input type="submit" name="submit" value="Drop" />
<input type="submit" name="submit" value="Cancel" />
</p>
</form>
"""

EXPIRED_TOKEN = """
<p>
Either your activation token is invalid or has expired and your requested
account has been deleted.  Please check that your token was entered correctly;
otherwise, re-create your account and activate it within %(token_lifetime)s.
</p>
"""

ACTIVATED_USER = """
<p> 
<b>Screen Name:</b> %(user.name)s 
</p>
<p> 
This user is already activated; you may now 
<a href="%(page_name)s?action=login">log in</a> as this user.  
</p>
"""

def user_activation_interface(request, form, token):
    """
    create the HTML that allows a new user to activate his/her
    via the token sent by email.
    """
    assert token

    faction = request.page.url(request, {"action": "activate"})
    _import_activation_module(request)

    # find the user based on the token
    target_user = activation.get_user_by_token(request, token,request.cfg.cache)
    if not target_user:
        data = activation.get_template_data(request, form_action=faction, 
                                            token=token)
        return EXPIRED_TOKEN % data
    if not activation.needs_activation(target_user):
        return ACTIVATED_USER % {'page_name': request.page.page_name,
                                 'user.name': target_user.name       }

    data = activation.get_template_data(request, target_user, 
                                        form_action=faction, token=token)
    return USER_ACTIVATION_FORM % data

SUPER_ACTIVATION_FORM = """
<p>
As a superuser, you can activate or drop any account awaiting activation. 
Marking an account as spam will drop the account with a record of why.
</p>
<form action="%(form_action)s" method="POST">
  <table>
    <tr>
      <th><b>Screen Name</b></th>
      <th><b>Email</b></th>
      <th><b>Full Name</b></th>
      <th><b>Institute</b></th>
      <th><b>Country</b></th>
      <th><b>Expires</b></th>
      <th><b>Cancel</b></th>
      <th><b>Drop</b></th>
      <th><b>Spam</b></th>
      <th><b>Activate</b></th>
    </tr>
%(rows)s
  </table>
%(submit)s
</form>
"""

SUPER_ACTIVATION_TABLE_ROW = """
    <tr>
      <td>%(user.name)s</td>
      <td>%(user.email)s</td>
      <td>%(user.firstname)s %(user.midname)s %(user.lastname)s</td>
      <td>%(user.institute)s</td>
      <td>%(user.country)s</td>
      <td>%(user.token_expires)s</td>
      <td align="center"><input type="radio" name="%(user.id)s" value="cancel"%(cancel_checked)s%(cancel_disabled)s></td>
      <td align="center"><input type="radio" name="%(user.id)s" value="drop"%(drop_checked)s></td>
      <td align="center"><input type="radio" name="%(user.id)s" value="spam"%(spam_disabled)s></td>
      <td align="center"><input type="radio" name="%(user.id)s" value="activate"%(activate_disabled)s></td>
    </tr>
"""

NOT_SUPER_USER = """
<p>
There are no accounts you are authorized to activate.  Note that you 
must be logged in as a superuser to activate accounts.  
</p>
"""

def super_activation_interface(request, form):
    """
    create the HTML that allows a superuser to activate any/all of the 
    pending accounts.
    """
    _import_activation_module(request)
    _ = request.getText
    
    faction = request.page.url(request, {"action": "activate"})
    if not request.user.isSuperUser() or not request.user.valid:
        # show all pending requests
        data = activation.get_template_data(request)
        return NOT_SUPER_USER % request

    # purge expired users
    activation.purge_expired_users(request)

    # collect the pending accounts
    checkable = ['Activate', 'Cancel', 'Drop', 'Spam']
    rows = []
    for uid in userutil.getUserList(request):
        user = userutil.User(request, uid)
        if activation.needs_activation(user):
            activation.cache_token2id(request.cfg.cache, user.token, uid)
            data = activation.get_template_data(request, user)
            data.update(map(lambda c: (c.lower()+'_checked',''), checkable))
            data.update(map(lambda c: (c.lower()+'_disabled',''), checkable))
                                                
            action = form.get(uid, [None])[0]
            if action in checkable:
                data[action.lower()+'_checked'] = ' checked="1"'
            rows.append(SUPER_ACTIVATION_TABLE_ROW % data)

    trows = "\n".join(rows)
    data = activation.get_template_data(request, request.user, 
                                        form_action=faction)
    data['rows'] = trows

    
    if len(rows) == 0:
        data['submit'] = "\n<p>There are no unactivated accounts.</p>"
    else:
        data['submit'] = '\n<p>\n' + \
            '<input type="submit" name="submit" value="submit">\n' + \
            '<input type="reset" name="reset" value="reset">\n</p>'
            
    return SUPER_ACTIVATION_FORM % data

#
# this function is not needed if this macro is made built-in
#    
def _import_activation_module(request):
    global activation
    if not activation:
        from MoinMoin.wikiutil import importPlugin
        activation = importPlugin(request.cfg, "activation", 
                                  "common", "import_module")(request.cfg)

    
